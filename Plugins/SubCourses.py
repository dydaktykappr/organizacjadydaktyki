#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import re

from PyQt4 import QtGui

from Plugins.SubCourses_files import SubCoursesDatabaseHandler
from Plugins.SubCourses_files import SubCourses_tab_ui as ui
from Plugins.abstract_plugin import AbstractPlugin


def getSubCoursesStrings(subCourseString):
    """
    Dzieli napis na napisy zawierające informacje o kursie towarzyszacym
    @param subCourseString: napis z pliku csv
    @return podzieloną listę
    """
    subcourses = [x.strip() for x in subCourseString.split(',')]
    subcourses = [x for x in subcourses if x]
    return subcourses


def parseSubCourseInfo(info):
    """
    parsuje informację o kursie towarzyszącym
    @param info: informacja
    @return słownik z nazwą i oceną z kursu
    """
    s = re.search("(.*)(\d+\.\d+)", info)
    result = {}
    result['name'] = s.group(1)
    result['note'] = float(s.group(2))
    return result


class KursyTowarzyszacePlugin(AbstractPlugin):
    """
    @brief klasa zakładki dla kursu towarzyszącego
    """

    def __init__(self, database, TabID=-1, parent=None):
        """

        @param database: uchwyt do bazy danych
        @param TabID: id zakładki
        @param parent: Qt parent
        """
        AbstractPlugin.__init__(self, parent, database, TabID)
        ## @var TabID
        # id zakładki
        self.TabID = TabID
        ## @var database
        # uchwyt bazy danych (lokalny)
        self.database = SubCoursesDatabaseHandler(database)
        ## @var mainHandler
        # uchwyt bazy danych (globalny)
        self.mainHandler = database
        self.ui = ui.Ui_tab()
        self.ui.setupUi(self)
        tabInfo = self.database.getTabInfo(TabID)
        ## @var courseID
        # id kursu
        self.courseID = tabInfo['course_id']
        ## @var index
        # indeks kursu w napisie o kursach towarzyszących (z csv)
        self.index = int(tabInfo['index'])
        self.configureTable()
        self.ui.filter.textChanged.connect(self.configureTableWithFilter)

    def configure(self):
        """
        konfiguruję zakładkę
        @return nazwę zakładki
        """
        return 'Kursy towarzyszące'

    def closeTab(self):
        """
            usuwa zakładkę z bazy danych
        """
        self.database.removeTab(self.TabID)

    def getNoteForStudent(self, StudentID):
        """
        Zwraca ocenę studenta
        @param StudentID: id studenta
        @return ocenę studenta
        """
        s_c_info = self.mainHandler.get_student_course_info(StudentID, self.courseID)
        sub_course_info = getSubCoursesStrings(s_c_info['sub_courses'])[self.index]
        note = parseSubCourseInfo(sub_course_info)['note']
        return note

    def configureTable(self):
        """
            Wypełnia tabelę
        """
        students_ids = self.mainHandler.getStudentsIDs(self.courseID)
        self.prepareHeader(len(students_ids))
        self.fillTable(students_ids)

    def configureTableWithFilter(self, filter):
        """
            Metoda wypełniająca tabelę studentami i ocenami
            @param filter - filtr dla listy studentów
        """
        students_ids = [i['ID'] for i in self.mainHandler.getStudentsFilterList(self.courseID, filter)]
        self.prepareHeader(len(students_ids))
        self.fillTable(students_ids)

    def prepareHeader(self, row_count):
        """
            Metoda przygotowuje nagłówek tabeli
            @param row_count - ilość wierszy w tabeli
        """
        columns_number = 4
        columns_header = ['Imiona', 'Nazwisko', 'Index', 'Ocena']
        self.ui.table.setColumnCount(columns_number)
        self.ui.table.setRowCount(row_count)
        self.ui.table.setHorizontalHeaderLabels(columns_header)

    def fillTable(self, student_ids):
        """
        Wypełnia tabelę
        @param student_ids: lista id studentów do wypełnienia
        """
        index = 0
        for s_id in student_ids:
            info = self.mainHandler.getStudentInfo(s_id)
            nameItem = QtGui.QTableWidgetItem(info['names'])
            surnameItem = QtGui.QTableWidgetItem(info['surname'])
            indexItem = QtGui.QTableWidgetItem(info['index'])
            note = self.getNoteForStudent(s_id)
            noteItem = QtGui.QTableWidgetItem(str(note))
            if note < 3:
                noteItem.setBackground(QtGui.QColor(250, 0, 0))
            column_index = 0
            column_index = self.appendItem(nameItem, index, column_index)
            column_index = self.appendItem(surnameItem, index, column_index)
            column_index = self.appendItem(indexItem, index, column_index)
            self.appendItem(noteItem, index, column_index)
            index += 1

    def appendItem(self, item, row_index, column_index):
        """
        uzupełnia pole wiersza
        @param item: QTableWidgetItem
        @param row_index: indeks wiersza
        @param column_index: indeks kolumny
        @return zinkrementowany indeks kolumny
        """
        self.ui.table.setItem(row_index, column_index, item)
        return column_index + 1

    @staticmethod
    def getTabIdList(database, CourseID):
        """
        Zwraca listę zakładek dla kursu
        @param database: uchwyt do bazy danych
        @param CourseID: id kursu
        @return listę zakładek (id)
        """
        databaseHandler = SubCoursesDatabaseHandler(database)
        databaseHandler.createSubCoursesListTable()
        return databaseHandler.getTabIdList(CourseID)

    @staticmethod
    def addId(database, CourseID):
        """
        dodaje nową zakładkę dla kursu
        @param database: uchwyt do bazy
        @param CourseID: id kursu
        @return id nowej zakładki
        """
        databaseHandler = SubCoursesDatabaseHandler(database)
        TabID = databaseHandler.addEmptyTab(CourseID)
        return TabID

    @staticmethod
    def create(CourseID, database, name, index):
        """
        Tworzy zakładkę
        @param CourseID:id kursu
        @param database: uchwyt do bazy
        @param name: nazwa kursu
        @param index: indeks kursu towarzyszącego w csv
        """
        Handler = SubCoursesDatabaseHandler(database)
        Handler.addTab(CourseID, name, index)
