#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from PyQt4 import QtGui, QtCore
from Plugins.Egzamin_files import Egzamin_tab_ui as ui
import logging
from Plugins.abstract_plugin import AbstractPlugin
from Plugins.Egzamin_files.egzaminDatabaseHandler import EgzaminDatabaseHandler
from Plugins.Egzamin_files import Egzamin_config_ui as config_ui
from Plugins.Egzamin_files import Egzamin_statistics_ui as statistics_ui
from gnuplotGraph import gnuplotGraph

#Widok tabeli konfiguracji
class QuestionsTableModel(QtCore.QAbstractTableModel):
    """
    @brief Klasa opisująca wygląd i działanie tabeli pytań w oknie konfiguracji.

    @details Klasa opisuję jak ma wyglądać oraz jak ma się zachowywać tabela pytań wyświetlona w oknie konfiguracji
    """
    def __init__(self, parent=None, *args):
        """
        @brief Klasa opisująca wygląd i działanie tabeli pytań dla okna konfiguracji.

        @details Klasa opisuję jak ma wyglądać oraz jak ma się zachowywać tabela wyświetlona w oknie konfiguracji
        """
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        ## @var arraydata
        # Dane w tabeli
        self.arraydata = []
        ## @var headerLabels
        # Nagłówki tabeli
        self.headerLabels=['Pytanie', 'Maks. pkt']
        ## @var columnNames
        # Nazwy kolumn w bazie danych
        self.columnNames=['ID','Question', 'MaxPoints']
        ## @var Parent
        # Rodzic
        self.Parent=parent

    def getVisibleRow(self):
        """
        Zwraca tabelę która jest widoczna (Niewidoczne są pola zaznaczone do usunięcia)
        @return Tabelę
        """
        newArray=[]
        for row in self.arraydata:
            if(str(row['ID'])=='new' or row['ID']>0):
                newArray.append(row)
        return newArray

    def rowCount(self, parent):
        """
        Zwraca liczbę wierszy
        @param parent Rodzic
        @return Liczbę wierszy
        """
        return len(self.getVisibleRow())

    def columnCount(self, parent):
        """
        Zwraca liczbę kolumn
        @param parent Rodzic
        @return Liczbę kolumn
        """
        if(len(self.arraydata)>0):
            return len(self.arraydata[0])-1
        else:
            return 0

    def data(self, index, role):
        """
        Zwraca dane danego pola
        @param index: Współrzędne danego pola
        @param role: Typ pytanej informacji np. kolor tła, wartość
        @return: Dane danego pola
        """
        if not index.isValid():
            return None
        elif role == QtCore.Qt.DisplayRole or role==QtCore.Qt.EditRole:
            return str( self.getVisibleRow()[index.row()][self.columnNames[index.column()+1]])
        else:
            return None

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        """
        @return Zwraca dane nagłówków tabeli
        """
        if role == QtCore.Qt.DisplayRole and orientation == QtCore.Qt.Horizontal:
            return self.headerLabels[section]
        return QtCore.QAbstractTableModel.headerData(self, section, orientation, role)

    def flags(self, index):
        """
        Ustawia flagi edytowania, zaznaczania itp.
        @param index: Współrzędne pola
        @return Flagi
        """
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable

    def setData(self, index, value, role):
        """
        Wpisuje dane do tabeli
        @param index: Współrzędne edytowanego pola
        @param value: Wartość
        @return True jeśli wszystko się powiodło
        """
        row=self.getVisibleRow()[index.row()]
        newIndex=self.arraydata.index(row)
        self.arraydata[newIndex][self.columnNames[index.column()+1]]=value
        self.Parent.updateScoreTable()
        return True

    def insertRow(self, index):
        """
        Dodaje wiersz
        @param index: Współrzędne
        """
        self.arraydata.append({'ID':'new', 'Question':'Pyt', 'MaxPoints':5 })
        self.layoutChanged.emit()

    def delRow(self,index):
        """
        Usuwa wiersz
        @param index Współrzędne
        """
        row=self.getVisibleRow()[index]
        newIndex=self.arraydata.index(row)
        if index<len(self.arraydata) and index>=0:
            if(self.getVisibleRow()[index]['ID']!='new'):
                self.arraydata[newIndex]['ID']=-self.arraydata[newIndex]['ID'] #ujemne id oznacza rekord do usuniecia
            else:
                self.arraydata.remove( self.arraydata[newIndex])
            self.layoutChanged.emit()

    def loadData(self,data):
        """
        Ładuje dane do tabeli
        @param data: tablica z danymi
        """
        self.arraydata=data
        self.layoutChanged.emit()

class Egzamin_statistics(QtGui. QDialog):
    """
    @brief Definiuje wygląd okna statystyk egzaminu
    @details Definiuje wygląd okna statystyk egzaminu
    """
    def __init__(self, parent=None, database=None, TabID=-1):
        """
        Metoda inicjująca instancję klasy.
        odpowiada za przypisanie wartości atrybutów klasy.

        @param parent Rodzic
        @param database Połączenie z bazą danych
        @param TabID Identyfikator zakładki
        """
        super(Egzamin_statistics, self).__init__(parent)
        ## @var database
        # Łącze do bazy danych
        self.database = database
        ## @var TabID
        # Identyfikator zakładki
        self.TabID = TabID

        ## @var ui
        # Zawiera wygląd okna
        self.ui = statistics_ui.Ui_Egzamin_statistics()
        self.ui.setupUi(self)
        self.ui.saveButton.clicked.connect(self.saveData)
        self.ui.cancelButton.clicked.connect(self.close)
        self.ui.cancel2Button.clicked.connect(self.close)
        self.ui.buttonHistogramNote.clicked.connect(self.saveHistogramNotes)
        self.ui.buttonHistogramSemester.clicked.connect(self.saveHistogramSemester)

    def saveData(self):
        """
        Tworzy plik tekstowy z danymi studentów, liczbą punktów i oceną z egzaminu
        """
        data=self.prepareTextData()
        openDialog = QtGui.QFileDialog(self)
        filename = openDialog.getSaveFileName(filter='*.txt')

        try:
            file = open(filename,'w')
            data = self.prepareTextData()

            info = ""
            if self.ui.checkBox_index.isChecked():
                info += "Index, "

            if self.ui.checkBox_surname.isChecked():
                info += "Nazwisko, "

            if self.ui.checkBox_name.isChecked():
                info += "Imiona, "

            if self.ui.checkBox_points.isChecked():
                info += "Punkty, "

            if self.ui.checkBox_note.isChecked():
                info += "Ocena"
            info += "\n"
            file.write(info)

            for row in data:
                if((not self.ui.checkBox_fail.isChecked()) and float(row['final'])<3):
                    continue
                if(not(self.ui.checkBox_pass.isChecked()) and float(row['final'])>=3):
                    continue

                ljust = 0

                info=""
                if self.ui.checkBox_index.isChecked():
                    info+="{},".format(row['index'])
                    ljust+=10

                if self.ui.checkBox_surname.isChecked():
                    info += "{},".format(row['surname'])
                    ljust+=15

                if self.ui.checkBox_name.isChecked():
                    info += "{}".format(row['names'])
                    ljust += 15
                ljust += 10
                info=info.ljust(ljust, '.')


                if self.ui.checkBox_points.isChecked():
                    for point in row['questions']:
                        info += "{},".format(point['Value'])
                        ljust += 2
                ljust += 10
                info = info.ljust(ljust, '.')

                if self.ui.checkBox_note.isChecked():
                    info += "{} ".format(row['final'])

                info+="\n"
                file.write(info)
        except:
            pass

    def prepareTextData(self):
        data=[]
        studentIDs=self.database.parentDatabase.getStudentsIDs(self.database.getCourseID(self.TabID))
        questions = self.database.getQuestions(self.TabID)

        for studentID in studentIDs:
            questionsNotes = []
            tmp=self.database.parentDatabase.getStudentInfo(studentID)

            for question in questions:
                questionsNotes.append(self.database.getNotes(studentID,question['ID']))

            tmp['questions']=questionsNotes
            tmp['final']=self.calculateFinalNote(questionsNotes)
            data.append(tmp)
        return data

    def calculateFinalNote(self,notes):
        points=0
        for note in notes:
            points+=note['Value'];
        return self.database.calculateNote(self.TabID, points)

    def saveHistogramNotes(self):
        studentIDs=self.database.parentDatabase.getStudentsIDs(self.database.getCourseID(self.TabID))
        questions = self.database.getQuestions(self.TabID)
        questionsNotes=[]
        final=[]
        for studentID in studentIDs:
            questionsNotes = []
            for question in questions:
                questionsNotes.append(self.database.getNotes(studentID, question['ID']))

            final.append(self.calculateFinalNote(questionsNotes))

        data=[]

        for note in sorted(set(final)):
            num=final.count(note)
            data.append([note,num])

        openDialog = QtGui.QFileDialog(self)
        filename = openDialog.getSaveFileName(filter='*.png')
        try:
            graph = gnuplotGraph()
            graph.gnuplotScript='gnuplotScripts/Histogram.gp'
            graph.filename=filename
            graph.data=data
            graph.dataLabels=['-','-']
            graph.title='Histogram ocen z egzaminu'
            graph.xlabel="Ocena"
            graph.ylabel="Ilość"
            graph.generateGraph()
        except:
            pass


    def saveHistogramSemester(self):
        studentIDs = self.database.parentDatabase.getStudentsIDs(self.database.getCourseID(self.TabID))
        studentsInfo=[]
        for studentID in studentIDs:
            studentsInfo.append(self.database.parentDatabase.get_student_course_info(studentID, self.database.getCourseID(self.TabID)))

        semesters= set()
        for dic in studentsInfo:
            semesters.add(dic['semestr'])

        questions = self.database.getQuestions(self.TabID)
        data = []
        for semester in sorted(semesters):
            studentIDs = self.database.parentDatabase.getStudentIdsFromSemester(self.database.getCourseID(self.TabID),semester)
            questionsNotes = []
            final = []
            for studentID in studentIDs:
                questionsNotes = []
                for question in questions:
                    questionsNotes.append(self.database.getNotes(studentID, question['ID']))
                final.append(self.calculateFinalNote(questionsNotes))

            data.append([semester,min(final),sum(final)/len(final),max(final)])

        openDialog = QtGui.QFileDialog(self)
        filename = openDialog.getSaveFileName(filter='*.png')
        try:
            graph = gnuplotGraph()
            graph.gnuplotScript = 'gnuplotScripts/Histogram.gp'
            graph.filename = filename
            graph.data = data
            graph.dataLabels = ['Semestr', 'Min','Średnia','Max']
            graph.title = 'Histogram ocen z egzaminu w zależności od semestru'
            graph.xlabel="Semestr"
            graph.ylabel="Ocena"
            graph.generateGraph()
        except:
            pass


class Egzamin_config(QtGui. QDialog):
    """
    @brief Definiuje wygląd okna ustawień egzaminu
    @details Definiuje wygląd okna ustawień egzaminu
    """
    def __init__(self, parent=None, database=None, TabID=-1):
        """
        Metoda inicjująca instancję klasy.
        odpowiada za przypisanie wartości atrybutów klasy.

        @param parent Rodzic
        @param database Połączenie z bazą danych
        @param TabID Identyfikator zakładki
        """
        super(Egzamin_config, self).__init__(parent)
        ## @var ui
        # Zawiera wygląd okna
        self.ui = config_ui.Ui_Egzamin_Config()
        self.ui.setupUi(self)
        ## @var database
        # Łącze do bazy danych
        self.database = database
        ## @var TabID
        # Identyfikator zakładki
        self.TabID = TabID
        ## @var TabName
        # Nazwa zakładki
        self.TabName=self.database.getTabName(TabID)
        self.ui.TabName.setText(self.TabName)
        self.ui.Apply.clicked.connect(self.apply)
        self.ui.AddRow.clicked.connect(self.addRow)
        self.ui.RemoveRow.clicked.connect(self.delRow)
        ## @var tablemodel
        # tabela pytań
        self.tablemodel = QuestionsTableModel(self)
        self.ui.tableView.setModel(self.tablemodel)
        self.tablemodel.loadData(self.database.getQuestions(self.TabID))

        self.ui.tableWidget.setColumnCount(3)
        self.ui.tableWidget.setRowCount(7)
        self.ui.tableWidget.setHorizontalHeaderLabels(["Ocena", "Min", "Max"])

        # Wstawiam listę ocen z zakładce punktacja
        for i in range(7):
            item = QtGui.QTableWidgetItem(str(2 + (i+ (i>0))* 0.5))
            item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget.setItem(i, 0, item)
            #Dodaje resztę pól
            item = QtGui.QTableWidgetItem()
            self.ui.tableWidget.setItem(i, 1, item)
            item = QtGui.QTableWidgetItem()
            item.setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)
            self.ui.tableWidget.setItem(i, 2, item)
            self.ui.tableWidget.cellChanged.connect(self.calculateMinPoints)

        #Blokuje możliwosc edycji min dla 2.0
        self.ui.tableWidget.item(0, 1).setFlags(QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEnabled)

        self.loadScore()
        self.updateScoreTable()


            #    widget = QtGui.QWidget()
#    pCheckBox = QtGui.QCheckBox()
#    pLayout = QtGui.QHBoxLayout(widget)
#    pLayout.addWidget(pCheckBox)
#    pLayout.setContentsMargins(0, 0, 0, 0)
#    widget.setLayout(pLayout)
#    self.ui.tableWidget.setCellWidget(1, 1, widget)

    def calculateMinPoints(self):
        """
        Automatycznie wylicza minimalną ilość punktów dla danej oceny
        @return Obliczoną ocenę
        """
        try:
            row=self.ui.tableWidget.selectedItems()[0].row()
            Points=self.ui.tableWidget.item(row,1).text()
            self.ui.tableWidget.item(row-1, 2).setText(Points)
        except:
            pass

    def sumPoints(self):
        """
        Oblicza maksymalną możliwą ilość punktów do zdobycia ze wszystkich pytań
        @return Obliczoną sumę
        """
        MaxPoints=0;
        for i in self.tablemodel.arraydata:
            MaxPoints+=int(i['MaxPoints'])
        return MaxPoints

    def updateScoreTable(self):
        """
        Aktualizuję tabelę Punkty-Ocena (Maksymalną możliwą ilość punktów do zdobycia)
        """
        self.ui.tableWidget.item(6, 2).setText(str(self.sumPoints()))

    def saveScore(self):
        """
        Zapisuję dane Punkty-Ocena do bazy danych
        """
        data=[]
        for i in range(7):
            Note=self.ui.tableWidget.item(i, 0).text()
            MinPoints = self.ui.tableWidget.item(i, 1).text()
            MaxPoints = self.ui.tableWidget.item(i, 2).text()
            data.append({'Note':Note , 'MinPoints':MinPoints , 'MaxPoints':MaxPoints})
        self.database.updatePointsTable(self.TabID,data)

    def loadScore(self):
        """
        Ładuję z bazy danych tabelę Punkty-Ocena
        """
        data=self.database.getPointsTable(self.TabID)
        for i in range(len(data)):
            self.ui.tableWidget.item(i, 0).setText(str(data[i]['Note']))
            self.ui.tableWidget.item(i, 1).setText(str(data[i]['MinPoints']))
            self.ui.tableWidget.item(i, 2).setText(str(data[i]['MaxPoints']))

    def apply(self):
        """
        Zapisuję dane do tabeli oraz zamyka okno
        """
        self.TabName = self.ui.TabName.text()
        self.database.setTabName(self.TabID, self.TabName)
        self.database.updateQuestions(self.TabID,self.tablemodel.arraydata)
        self.saveScore()
        self.close()

    def addRow(self):
        """
        Dodaję wiersz do tabeli pytań
        """
        self.tablemodel.insertRow(self.tablemodel.rowCount(None))
        self.updateScoreTable()

    def delRow(self):
        """
        Usuwa wiersz z tabeli pytań
        """
        index=self.ui.tableView.selectionModel().selection().indexes()
        if(len(index)>0):
            self.tablemodel.delRow(index[0].row())
            self.updateScoreTable()


class ExamTableModel(QtCore.QAbstractTableModel):
    """
    @brief Klasa opisująca wygląd i działanie tabeli.

    @details Klasa opisuję jak ma wyglądać oraz jak ma się zachowywać tabela wyświetlona na zakładce
    """
    def __init__(self, database, TabID, parent=None, *args):
        """
        Metoda inicjująca instancję klasy.
        odpowiada za przypisanie wartości atrybutów klasy.

        @param database: Połączenie z bazą danych
        @param TabID: Identyfikator zakładki
        @param parent: Rodzic
        @param args: dodatkowe parametry
        """
        QtCore.QAbstractTableModel.__init__(self, parent, *args)
        ## @var database
        # Połączenie z bazą danych
        self.database = database

        ## @var TabID
        # Identyfikator zakładki
        self.TabID = TabID

        ## @var CourseID
        #Identyfikator kursu
        self.CourseID = database.getCourseID(TabID)
        #Nagłówki
        self.PrepareHeaders()
        #dane w tabeli
        self.FilterData("")

    def PrepareHeaders(self):
        """
        Przygotowuję nagłówki tabeli
        """

        ## @var header_labels
        # Zawiera etykiety nagłówków tabeli
        self.header_labels = ['Imiona', 'Nazwisko', 'Index']
        questions = self.database.getQuestions(self.TabID)
        for question in questions:
            self.header_labels.append(question['Question'])
        self.header_labels.append('Suma')
        self.header_labels.append('Ocena')

    def FilterData(self,Filter):
        """
        Filtruję studentów według wzorca
        @param Filter: Wzorzec
        """
        questions = self.database.getQuestions(self.TabID)
        students = self.database.parentDatabase.getStudentsFilterList(self.CourseID, Filter)
        ## @var arraydata
        # Zawiera dane tabeli
        self.arraydata = []
        for student in students:
            row = [{'Value': student['names']}, {'Value': student['surname']}, {'Value': student['index']}]
            for question in questions:
                row.append(self.database.getNotes(student['ID'], question['ID']))
            self.arraydata.append(row)
        self.layoutChanged.emit()

    def rowCount(self, parent):
        """
        Zwraca liczbę wierszy
        @param parent: Rodzic
        @return Liczbę wierszy
        """
        return len(self.arraydata)

    def columnCount(self, parent):
        """
        Zwraca liczbę kolumn
        @param parent: Rodzic
        @return Liczbę kolumn
        """
        if(len(self.arraydata)>0):
            return len(self.arraydata[0])+2 #dodaje sume oraz ocene
        else:
            return 0

    def data(self, index, role):
        """
        Zwraca dane danego pola
        @param index: Współrzędne danego pola
        @param role: Typ pytanej informacji np. kolor tła, wartość
        @return: Dane danego pola
        """
        if not index.isValid():
            return None
        elif role == QtCore.Qt.BackgroundRole:
            #tu ustawiam kolorki tła
            if(index.column()==self.columnCount(None)-1 #Sprawdzam czy to ostatnia kolumna
               and self.getNote(index.row())==2):                  #oraz czy ocena jest równa 2
                return QtGui.QBrush(QtCore.Qt.red)
            else:
                return None
        elif role != QtCore.Qt.DisplayRole and role!=QtCore.Qt.EditRole:
            return None
        if(index.column()==self.columnCount(None)-2): #tu wyświetlam sumę
            value=self.sumPoints(index.row())
        elif(index.column()==self.columnCount(None)-1): #tu wuświetlam ocenę
            value=self.getNote(index.row())
        else:
            value=self.arraydata[index.row()][index.column()]['Value']
        if(value!=-1):
            return str(value)
        else:
            return ""

    def getNote(self,row):
        """
        Zwraca obliczoną ocenę dla danego wiersza
        @param row: Numer wiersza
        @return Obliczoną ocenę
        """
        points=self.sumPoints(row)
        return self.database.calculateNote(self.TabID, points)

    def sumPoints(self,row):
        """
        Oblicza sumę punktów
        @param row: Numer wiersza
        @return Sumę punktów
        """
        value=0
        for i in range(3,self.columnCount(None)-2): # 6 bo nie biorę pod uwagę id,imiona,nazwiska,indeksu,sumy,oceny
            try:
               value+=float(self.arraydata[row][i]['Value'])
            except:
                pass
        return value


    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        """
        @return Zwraca dane nagłówków tabeli
        """
        if role == QtCore.Qt.DisplayRole and orientation == QtCore.Qt.Horizontal:
            return self.header_labels[section]
        return QtCore.QAbstractTableModel.headerData(self, section, orientation, role)

    def flags(self, index):
        """
        Ustawia flagi zaznaczania, edytowania itp.
        """
        return QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable | QtCore.Qt.ItemIsEditable

    def setData(self, index, value, role):
        """
        Wpisuje dane do tabeli
        @param index: Współrzędne edytowanego pola
        @param value: Wartość
        @return True jeśli wszystko się powiodło
        """
        #Ma nie działać edytowanie studentów
        if(index.column()<3):
            return True;
        self.arraydata[index.row()][index.column()]['Value']=value
        cell=self.arraydata[index.row()][index.column()]
        self.database.updateNote(cell['ID'],cell['Value'])
        return True


class EgzaminPlugin(AbstractPlugin):
    """
    @brief Klasa dla wtyczki egzaminu.

    @details Zawiera tabelę opisującą egzamin
    Klasa obowiązkowo dziedziczy po AbstractPlugin.
    """
    def __init__(self, database, TabID=-1, parent=None):
        """
        Metoda inicjująca instancję klasy.
        odpowiada za przypisanie wartości atrybutów klasy.

        @param database połączenie z bazą danych dające dostęp do metod klasy egzaminDatabaseHandler
        @param TabID unikalny numer identyfikacyjny zakładki (pozwala na pracę z wieloma zakładkami jednocześnie)
        @param parent rodzic
        """
        AbstractPlugin.__init__(self, parent, database, TabID)

        ## @var TabID
        # unikalny numer identyfikacyjny zakładki (pozwala na pracę z wieloma zakładkami jednocześnie)
        self.TabID = TabID

        ## @var database
        # połączenie z bazą danych dające dostęp do metod klasy obecnoscDatabaseHandler
        self.database = EgzaminDatabaseHandler(database)

        ##@var ui
        # plik interfejsu graficznego Qt, można go zmieniać zewnętrznie za pomocą QtDesigner
        self.ui = ui.Ui_tab()
        self.ui.setupUi(self)

        ##@var tablemodel
        # Model wyglądu wyświetlanej tabeli
        self.tablemodel = ExamTableModel(self.database,TabID)
        self.ui.tableView.setModel(self.tablemodel)
        self.ui.SearchText.textChanged.connect(self.SearchStudents)
        self.ui.Settings.clicked.connect(self.configure)
        self.ui.Statistics.clicked.connect(self.statistics)

    def configure(self):
        """
        Metoda ta powoduję otwarcie okna konfiguracji

        @return Nazwę utworzonej zakładki
        """
        config = Egzamin_config(self, self.database, self.TabID)
        config.exec_()
        self.tablemodel.PrepareHeaders()
        self.tablemodel.FilterData("")
        return config.TabName

    def statistics(self):
        """
        Metoda ta powoduję otwarcie okna statystyk egzaminu
        """
        config = Egzamin_statistics(self, self.database, self.TabID)
        config.exec_()

    def closeTab(self):
        """
        Usuwa zakładkę z bazy danych
        """
        self.database.removeTab(self.TabID)

    @staticmethod
    def getTabIdList(database, CourseID):
        """
        Pobiera listę dostępnych zakładek typu egzamin
        @param database Połączenie do bazy danych
        @param CourseID Identyfikator kursu
        @return listę zakładek
        """
        db = EgzaminDatabaseHandler(database)
        db.createExamListTable()
        db.createExamQuestionsTable()
        db.createExamNotesTable()
        db.createExamPointsTable()
        return db.getTabIdList(CourseID)

    @staticmethod
    def addId(database, CourseID):
        """
        Dodaję pustą zakładkę do bazy danych
        @param database Połączenie do bazy danych
        @param CourseID Identyfikator kursu
        @return Identyfikator nowo dodanej zakładki
        """
        db = EgzaminDatabaseHandler(database)
        TabID=db.addEmptyTab(CourseID)
        db.fillPointsTable(TabID)
        return TabID

    def SearchStudents(self,Text):
        """
        Wypełnia tabelę studentami pasującymi do wzorca Text
        @param Text Wzorzec
        """
        self.tablemodel.FilterData(Text)

    def getNoteForStudent(self, StudentID):
        """
        Oblicza ocenę studenta na podstawie sumy zdobytych punktów
        @param StudentID Identyfikator  studenta
        @return Ocenę
        """
        sum=0
        questions=self.database.getQuestions(self.TabID)
        for question in questions:
            sum+=self.database.getNotes(StudentID, question['ID'])['Value']
        Note = self.database.calculateNote(self.TabID,sum)
        return Note
