class SubCoursesDatabaseHandler:
    """
    @brief Obsługa bazy danych dla kursu towarzyszącego
    """
    def __init__(self, database):
        """

        @param database: uchwyt do bazy
        """
        self.database = database.getDatabase()
        self.databaseMainHandler = database

    def getTabIdList(self, CourseID):
        """
        zwraca listę zakładek dla kursu
        @param CourseID: id kursu
        @return listę zakładek
        """
        sql = "select `ID`, `Name` from `SubCoursesList` where `CourseID`='{}'".format(CourseID)
        cursor = self.database.execute(sql)
        result = []
        for row in cursor:
            result.append({'id': row[0], 'name': row[1]})
        return result

    def createSubCoursesListTable(self):
        """
        Tworzy tabelę wiąząca kurs i zakładkę
        """
        sql = """CREATE TABLE IF NOT EXISTS `SubCoursesList` (
        `ID` INTEGER PRIMARY KEY AUTOINCREMENT,
        `CourseID` INTEGER NOT NULL,
        `Name` text NOT NULL,
        `Index` INTEGER NOT NULL
        );"""
        self.database.execute(sql)
        self.database.commit()

    def addEmptyTab(self, CourseID):
        """
        dodaje pustą zakładkę
        @param CourseID: id kursu
        """
        sql = "INSERT INTO `SubCoursesList` (`CourseID`,`Name`, `Index`) VALUES ('{}', 'Nazwa', '0')".format(CourseID)
        cursor = self.database.execute(sql)
        self.database.commit()
        return cursor.lastrowid

    def addTab(self, CourseID, name, index):
        """
        dodaje zakładkę
        @param CourseID: id kursu
        @param name: nazwa zakładki
        @param index: indeks kursu towarzyszącego w csv
        """
        self.createSubCoursesListTable()
        sql = "INSERT INTO `SubCoursesList` (`CourseID`, `Name`, `Index`) VALUES ('{}', '{}', '{}')".format(CourseID,
                                                                                                            name, index)
        cursor = self.database.execute(sql)
        self.database.commit()
        return cursor.lastrowid

    def removeTab(self, tabID):
        """
        Usuwa zakładkę z bazy danych
        @param tabID: id zakładki
        """
        sql = "DELETE FROM `SubCoursesList` WHERE `ID`= '{}'".format(tabID)
        self.database.execute(sql)
        self.database.commit()

    def getTabInfo(self, TabId):
        """
        Zwraca słownik informacji o zakładcę
        @param TabId: id zakładki
        @return słownik, pola:\n
        course_id\n
        name\n
        index
        """
        command = 'select `CourseID`, `Name`, `Index` from `SubCoursesList` where `ID`={}'.format(TabId)
        tabInfo = self.database.execute(command)
        tabInfo = tabInfo.fetchone()
        return {'course_id': tabInfo[0], 'name': tabInfo[1], 'index': tabInfo[2]}

