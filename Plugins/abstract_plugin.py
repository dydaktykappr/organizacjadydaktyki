#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from PyQt4 import QtGui
from abc import abstractmethod
import random


class NoteNotAvailable(Exception):
    """
    @brief Klasa wyjątku
    @details Klasa wyjątku który jest rzucany jeśli zakładka nie zwraca oceny
    """
    pass


class AbstractPlugin(QtGui.QWidget):
    """
    @brief Abstrakcyjna klasa pluginu
    @details Abstrakcyjna klasa pluginu która definiuję podstawowe metody
    """
    def __init__(self, parent, database, TabID):

        QtGui.QWidget.__init__(self, parent)
        super(AbstractPlugin, self).__init__(parent)
        self.database = database
        self.ID = TabID

    @abstractmethod
    def configure(self):
        """
        Metoda abstrakcyjna wykonywana przy tworzeniu nowego pluginu, powinna uruchomić okno konfiguracji
        @return Nazwa zakładki
        """
        pass

    @abstractmethod
    def closeTab(self):
        """
        Abstrakcyjna metoda uruchomiona przy zamykaniu zakładki
        @return:
        """
        pass

    @staticmethod
    @abstractmethod
    def getTabIdList(database, CourseID):
        """
        Metoda statyczna abstrakcyjna która zwraca listę zakładek danego typu
        @param database: Połączenie z bazą danych
        @param CourseID: Identyfikator kursu
        @return Listę identyfikatorów zakładek
        """
        return []

    @staticmethod
    @abstractmethod
    def addId(courseID):
        """
        Metoda statyczna abstrakcyjna która jest uruchamiana przy tworzeniu nowego pluginu
        @param courseID: Identyfikator kursu
        @return Nazwę nowej zakładki
        """
        pass

    @abstractmethod
    def getNoteForStudent(self, StudentID):
        """
        Zwraca ostateczną ocenę z danego pluginu dla studenta
        @param StudentID: Identyfikator studenta
        @return: Ocenę
        """
        return random.randint(2, 5)


