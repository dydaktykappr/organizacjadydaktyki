#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sqlite3
from PyQt4 import QtCore, QtGui
from Plugins.Obecnosc_files import Obecnosc_tab_ui as ui
import re
import logging
from Plugins.abstract_plugin import AbstractPlugin, NoteNotAvailable
from Plugins.Obecnosc_files.obecnoscDatabaseHandler import obecnoscDatabaseHandler
database = None


class ObecnoscPlugin(AbstractPlugin):
    """
    @brief Klasa dla wtyczki obecności.

    @details Zawiera tabelę podsumowującą dotychczasową frekwencję oraz przycisk pozwalający na wprowadzenie nowej listy obecności.
    Klasa obowiązkowo dziedziczy po AbstractPlugin.
    """
    def __init__(self, database, TabID=-1, parent=None):
        """
        Metoda inicjująca instancję klasy.
        Automatycznie wywołana przy tworzeniu zakładki; odpowiada za przypisanie wartości atrybutów klasy.

        @param database połączenie z bazą danych dające dostęp do metod klasy obecnoscDatabaseHandler
        @param TabID unikalny numer identyfikacyjny zakładki (pozwala na pracę z wieloma zakładkami jednocześnie)
        @param parent rodzic nowej zakładki
        """
        AbstractPlugin.__init__(self, parent, database, TabID)

        ## @var TabID
        # unikalny numer identyfikacyjny zakładki (pozwala na pracę z wieloma zakładkami jednocześnie)
        self.TabID = TabID

        ## @var database
        # połączenie z bazą danych dające dostęp do metod klasy obecnoscDatabaseHandler
        self.database = obecnoscDatabaseHandler(database)

        ##@var ui
        # plik interfejsu graficznego Qt, można go zmieniać zewnętrznie za pomocą QtDesigner
        self.ui = ui.Ui_tab()

        ##@var CourseID
        #  unikalny numer identyfikacyjny kursu, do którego odnosi się dana zakładka
        self.CourseID = database.getCourse(TabID)

        self.ui.setupUi(self)
        ## @var ui.NewListButton
        # przycisk wywołujący stworzenie nowej listy obecności (atrybut interfejsu graficznego).
        self.ui.NewListButton.clicked.connect(self.new_checklist)

        self.setHistoricalAttendanceList()


    def setHistoricalAttendanceList(self):
        """
        Metoda tworząca i wypełniająca podsumowanie frekwencji na zajęciach, działa na zdefiniowanym w .ui
        QTableWidget @ref HistoricalAttendanceList. Wprowadza zmiany w liście @var Students, wypełniając ją danymi studentów
        zapisanych na dany kurs.
        @return wprowadza zmiany w liście studentów Students oraz w wyświetlonym oknie.
        """

        ## @var Students
        #  lista studentów; dane każdego studenta są zapisane w słowniku o kluczach: @a ID, @a index, @a name, @a surname,
        # gdzie ID jest identyfikatorem studenta w bazie danych.
        self.Students = []

        ## @var Stud_IDs
        # lista przechowująca numery identyfikacyjne studentów zapisanych na kurs
        Stud_IDs = self.database.getStudentsIDs(self.CourseID['id'])
        for ID in Stud_IDs:
            Stud_info = self.database.getStudentInfo(ID)
            Stud_info['ID'] = ID
            self.Students.append(Stud_info)

        ## @var header_labels
        # lista nagłówków dla stałych kolumn tabeli; jest rozszerzana o daty kolejnych zajęć
        self.header_labels = ['Imiona', 'Nazwisko', 'Indeks', 'Frekwencja']

        ## @var HistoricalList
        # zdefiniowany w pliku ui obiekt typu QTableWidget, wyświetlający podsumowanie dotychczas wprowadzonych list obecności.

        self.ui.HistoricalList.insertColumn(0)
        self.ui.HistoricalList.insertColumn(1)
        self.ui.HistoricalList.insertColumn(2)
        self.ui.HistoricalList.insertColumn(3)

        for student in self.Students:
            i = student['ID']-1
            self.ui.HistoricalList.insertRow(i)
            self.ui.HistoricalList.setItem(i, 0, QtGui.QTableWidgetItem(student['names']))
            self.ui.HistoricalList.setItem(i, 1, QtGui.QTableWidgetItem(student['surname']))
            self.ui.HistoricalList.setItem(i, 2, QtGui.QTableWidgetItem(student['index']))

        ## @var days
        # lista dat, dla których zapisano listy obecności danego kursu
        days = self.database.getAttendanceDaysByCourse(self.CourseID['id'])

        column = 4
        for day in days:

            self.header_labels.append(day)
            self.ui.HistoricalList.insertColumn(column)
            attended = self.database.getAttendanceByLecture(self.CourseID['id'], day)

            for student in attended:
                ID = student['StudentID']-1
                presence = student['Attended']
                self.ui.HistoricalList.setItem(ID, column, QtGui.QTableWidgetItem(presence))
                self.ui.HistoricalList.item(ID, column).setTextAlignment(4)
                if presence == "nieobecność":
                    self.ui.HistoricalList.item(ID, column).setBackground(QtGui.QColor(255, 69, 0))
                elif presence == "usprawiedliwienie":
                    self.ui.HistoricalList.item(ID, column).setBackground(QtGui.QColor(218, 165, 32))
                elif presence == "obecność":
                    self.ui.HistoricalList.item(ID, column).setBackground(QtGui.QColor(0, 128, 0))
            column += 1

        columns = column - 4
        if columns:
            for student in attended:
                ob = 0
                nob = 0
                uspr = 0
                for day in range(columns):
                    if self.ui.HistoricalList.item(student['StudentID']-1, day+4).text() == "obecność":
                        ob += 1
                    elif self.ui.HistoricalList.item(student['StudentID']-1, day+4).text() == "nieobecność":
                        nob += 1
                    elif self.ui.HistoricalList.item(student['StudentID']-1, day+4).text() == "usprawiedliwienie":
                        uspr += 1
                try:
                    frekwencja = str((ob/(ob+nob+uspr))*100) + '% (w tym ' + str(uspr) + ' uspr)'
                except ZeroDivisionError:
                    frekwencja = 'Nieznana kategoria obecności'
                self.ui.HistoricalList.setItem(student['StudentID']-1, 3, QtGui.QTableWidgetItem(frekwencja))
                for stud in self.Students:
                    if stud['ID'] == student['StudentID']:
                        try:
                            stud['frekwencja'] = (ob/(ob+nob+uspr))*100
                        except ZeroDivisionError:
                            stud['frekwencja'] = 0


        self.ui.HistoricalList.setHorizontalHeaderLabels(self.header_labels)
        self.ui.HistoricalList.sortItems(1)
        for col in range(self.ui.HistoricalList.columnCount()):
            self.ui.HistoricalList.horizontalHeader().setResizeMode(col, QtGui.QHeaderView.ResizeToContents)
        self.ui.HistoricalList.cellClicked.connect(self.changePresence)

    def changePresence(self, row, col):
        """
        Metoda pozwalająca zmienić status obecności studenta na zatwierdzonej liście (wprowadza zmiany do bazy danych).
        Jest wywoływana poprzez kliknięcie na daną komórkę w tabeli @ref HistoricalList.
        @param row numer wiersza
        @param col numer kolumny
        @return zmiana pola "obecność" danego studenta w danym dniu
        """
        if col >= 4:
            ## @var date
            # data zajęć wyciągnięta z nagłówka kolumny
            date = self.ui.HistoricalList.horizontalHeaderItem(col).text()
            ## @var index
            #  numer indeksu studenta wyciągnięty z trzeciej kolumny wiersza
            index = self.ui.HistoricalList.item(row, 2).text()
            # @var old_status
            # zastany status obecności, zapisywany na wypadek późniejszego przywrócenia
            old_status = self.ui.HistoricalList.item(row, col).clone()

            # poszukiwanie ID studenta na podstawie numeru indeksu. Da się to wyciągnąć samym numerem wiersza,
            # ale tak jest bezpieczniej na wypadek przyszłych zmian w kodzie
            for student in self.Students:
                if student['index'] == index:
                    ID = student['ID']
                    break

            status = self.Present(row, col)

            # Pop-up dla potwierdzenia zmian
            confirm = QtGui.QMessageBox.question(self, 'Zmienić status?',
                                                 "Wprowadzono " + status
                                                 + " dla " + self.ui.HistoricalList.item(row, 1).text()
                                                 + " " + self.ui.HistoricalList.item(row, 0).text()
                                                 + " w dniu " + date + ". \nZapisać?",
                                                 QtGui.QMessageBox.Yes |
                                                 QtGui.QMessageBox.No, QtGui.QMessageBox.No)

            # Jeśli potwierdzono, nowe dane idą do bazy
            if confirm == QtGui.QMessageBox.Yes:
                self.database.saveRow(ID, self.CourseID['id'], date, status)

            # Jeśli odrzucono, przywracamy wcześniejsze dane
            else:
                self.ui.HistoricalList.setItem(row, col, old_status)

            #        self.ui.SearchText.textChanged.connect(self.SearchStudents)

    def getNoteForStudent(self, StudentID):
        """
        Metoda zwracająca proponowaną ocenę za obecność.
        @param StudentID: identyfikator studenta
        @return proponowaną ocenę
        """
        for student in self.Students:
            if student['ID'] == StudentID:
                freq = student['frekwencja']
        note = freq/20
        if note < 3:
            note = 2.0
        elif note < 3.5:
            note = 3.0
        elif note < 4:
            note = 3.5
        elif note < 4.5:
            note = 4.0
        elif note < 5:
            note = 4.5
        else:
            note = 5.0
        return round(note, 1)

    def new_checklist (self):
        """
        Tworzy nowe okno klasy @ref NewList w odpowiedzi na sygnał z przycisku @ref NewListButton.

        Po zamknięciu okna odświeża tabelę @ref HistoricalList, uwzględniając nowo wprowadzone dane.
        """
        Lista = NewList(database=self.database,
                        TabID=self.TabID,
                        CourseID=self.CourseID,
                        parent=self)
        Lista.resize(650, 800)
        Lista.exec()

        self.ui.HistoricalList.setRowCount(0)
        self.ui.HistoricalList.setColumnCount(0)
        self.setHistoricalAttendanceList()

    def closeTab(self):
        """
        Metoda zamykająca zakładkę.
        @return usuwa dane zakładki z bazy danych
        """
        self.database.removeTab(self.TabID)

    @staticmethod
    def getTabIdList(database, CourseID):
        """
        Metoda wywołująca tworzenie tablic obecności w bazie danych.
        @param database: łącze do bazy danych
        @param CourseID: identyfikator kursu
        @return identyfikatory zakładek z danego kursu
        """
        db = obecnoscDatabaseHandler(database)
        db.createAttendListTable()
        db.createAttendanceTable()
        return db.getTabIdList(CourseID)

    @staticmethod
    def addId(database, CourseID):
        """
        Metoda wywołująca tworzenie nowej zakładki.
        @param database: łącze do bazy danych
        @param CourseID: identyfikator kursu
        @return identyfikator nowej zakładki
        """
        db = obecnoscDatabaseHandler(database)
        return db.addEmptyTab(CourseID)

    def configure(self):
        """
        Metoda wykonywana przy tworzeniu nowej zakładki.
        @return nazwa stworzonej zakładki
        """
        return 'Obecność'

    def Present(self, row, col):
        """
        Metoda zmieniająca status obecności w klikniętej komórce (tylko lokalna zmiana, bez zapisu do bazy).
        Kolejność zmian: nieobecność -> usprawiedliwienie -> obecność.
        Działa na @ref HistoricalList.
        Uwaga! To nie jest to samo co NewList.Present()!
        @param row numer wiersza
        @param col numer kolumny
        @return słowny status obecności z listy [nieobecność, obecność, usprawiedliwienie]
        """

        if self.ui.HistoricalList.item(row, col).text() == "nieobecność":
            self.ui.HistoricalList.item(row, col).setBackground(QtGui.QColor(218, 165, 32))
            self.ui.HistoricalList.item(row, col).setText("usprawiedliwienie")

        elif self.ui.HistoricalList.item(row, col).text() == "usprawiedliwienie":
            self.ui.HistoricalList.item(row, col).setBackground(QtGui.QColor(0, 128, 0))
            self.ui.HistoricalList.item(row, col).setText("obecność")

        elif self.ui.HistoricalList.item(row, col).text() == "obecność":
            self.ui.HistoricalList.item(row, col).setBackground(QtGui.QColor(255, 69, 0))
            self.ui.HistoricalList.item(row, col).setText("nieobecność")

        return self.ui.HistoricalList.item(row, col).text()


class NewList(QtGui.QDialog):
    """
    Klasa odpowiedzialna za tworzenie i obsługę osobnego okna do wprowadzania nowej listy obecności.

    Instancja tej klasy jest tworzona na sygnał przycisku @ref NewListButton z zakładki Obecność.
    """
    def __init__(self, database, TabID, CourseID, parent=None):
        """
        Metoda inicjująca okno wprowadzania nowej listy obecności.
        @param database: łącze do wykorzystywanej bazy danych
        @param TabID: identyfikator zakładki macierzystej
        @param CourseID: identyfikator kursu
        @param parent: rodzic okna, należy zachować None aby okno otwierało się osobno
        """
        QtGui.QDialog.__init__(self, parent)
        ## @var TabID
        # Identyfikator zakładki macierzystej
        self.TabID = TabID
        ## @var database
        # Łącze do bazy danych poprzez @ref obecnoscDatabaseHandler
        self.database = database
        ## @var CourseID
        # Identyfikator kursu
        self.CourseID = CourseID['id']
        self.initUi()

    def initUi(self):
        """
        Metoda tworząca zawartość okna.
        @return wyświetla nowe okno
        """
        self.setWindowTitle("Lista obecności")

        ## @var StudentsList
        # QTableWidget zawierający listę uczestników kursu i kolumnę do zaznaczania obecności.
        self.StudentsList = QtGui.QTableWidget(0, 4)

        ## @var Students
        # Lista słowników z danymi wszystkich uczestników kursu (ID, imię, nazwisko, indeks).
        # Lista ta jest wykorzystywana do zapełnienia tabeli @ref StudentsList.
        self.Students = []

        Stud_IDs = self.database.getStudentsIDs(self.CourseID)
        for ID in Stud_IDs:
            Stud_info = self.database.getStudentInfo(ID)
            Stud_info['ID'] = ID
            self.Students.append(Stud_info)

        result = None
        while result is None:
            ## @var set_date
            # Osobne okienko typu QInputDialog do wprowadzania daty nowej listy obecności.
            # Wymaga podania daty w formacie dwucyfrowym dzień-miesiąc-rok i będzie ponawiane do momentu uzyskania
            # pasujacego wpisu. Wprowadzone dane są rozbijane na @var day, @var month i @var year za pomocą wyrażenia regularnego.
            # Obecnie rozbicie nie jest wykorzystywane, ale zostawiam na przyszłość, może się przyda.
            set_date = QtGui.QInputDialog()
            set_date.setOkButtonText("OK")
            set_date.setLabelText("Proszę podać datę zajęć w formacie DD-MM-RR")
            set_date.setWindowTitle("Data zajęć")
            set_date.exec()
            if set_date.result() == 0:
                break

            ## @var date
            # Przechowuje wprowadzoną datę zajęć.
            self.date = set_date.textValue()

            mask = re.compile('(\d\d)-(\d\d)-(\d\d)')
            result = re.match(mask, self.date)
            if result:
                day = result.group(1)
                month = result.group(2)
                year = result.group(3)
        if set_date.result() == 0:
            self.close()
            return 0

        header_labels = ['Nazwisko', 'Imiona', 'Indeks', "Obecność " + self.date]

        self.StudentsList.setHorizontalHeaderLabels(header_labels)

        for student in self.Students:
            ID = student['ID'] - 1
            self.StudentsList.insertRow(ID)
            self.StudentsList.setItem(ID, 0, QtGui.QTableWidgetItem(student['surname']))
            self.StudentsList.setItem(ID, 1, QtGui.QTableWidgetItem(student['names']))
            self.StudentsList.setItem(ID, 2, QtGui.QTableWidgetItem(student['index']))
            self.StudentsList.setItem(ID, 3, QtGui.QTableWidgetItem(0))
            self.StudentsList.item(ID, 3).setBackground(QtGui.QColor(255, 69, 0))
            self.StudentsList.item(ID, 3).setText("nieobecność")
            self.StudentsList.item(ID, 3).setTextAlignment(4)

        self.StudentsList.cellClicked.connect(self.Present)

        self.StudentsList.sortItems(0)
        self.StudentsList.horizontalHeader().setResizeMode(0, QtGui.QHeaderView.ResizeToContents)
        self.StudentsList.horizontalHeader().setResizeMode(1, QtGui.QHeaderView.ResizeToContents)
        self.StudentsList.horizontalHeader().setResizeMode(2, QtGui.QHeaderView.ResizeToContents)
        self.StudentsList.horizontalHeader().setResizeMode(3, QtGui.QHeaderView.Stretch)

        #create Save button
        ## @var SaveButton
        # Przycisk wywołujący zapisanie listy - metodę @ref save_checklist
        SaveButton = QtGui.QPushButton('Zatwierdź', self)
        SaveButton.resize(SaveButton.sizeHint())
        SaveButton.resize(SaveButton.sizeHint())
        SaveButton.clicked.connect(self.save_checklist)

        # create Cancel button
        ## @var CancelButton
        # Przycisk wywołujący zamknięcie okna bez zapisania zmian - metoda @ref closeEvent
        CancelButton = QtGui.QPushButton('Porzuć', self)
        CancelButton.resize(CancelButton.sizeHint())
        CancelButton.clicked.connect(self.closeEvent)

        hbox = QtGui.QHBoxLayout()
        hbox.addStretch(1)
        hbox.addWidget(CancelButton)
        hbox.addWidget(SaveButton)

        vbox = QtGui.QVBoxLayout()
        vbox.addWidget(self.StudentsList)
        vbox.addLayout(hbox)

        self.setLayout(vbox)

    def save_checklist(self):
        """
        Metoda zapisująca nową listę do bazy danych.
        @return zmiany w tabeli Attendance
        """
        reply = QtGui.QMessageBox.question(self, "Zapisać?", 'Zatwierdzić zmiany?',
                                           QtGui.QMessageBox.Yes | QtGui.QMessageBox.No,
                                           QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            for student in self.Students:
                self.database.saveRow(student['ID'],
                                      self.CourseID,
                                      self.date,
                                      self.StudentsList.item(student['ID']-1, 3).text())
            self.database.commit()
            self.close()

    def closeEvent(self):
        """
        Metoda zamykająca okno bez zapisania zmian.
        """
        reply = QtGui.QMessageBox.question(self, 'Porzucić zmiany?',
                                     "Czy na pewno odrzucić listę?", QtGui.QMessageBox.Yes |
                                           QtGui.QMessageBox.No, QtGui.QMessageBox.No)

        if reply == QtGui.QMessageBox.Yes:
            self.close()

    def Present(self, row, col):
        """
        Metoda obsługująca kliknięcia w kolumnie "obecność" @ref StudentsList.
        Zmienia status studenta w kolejności nieobecność -> obecność -> usprawiedliwienie.
        Uwaga: metoda jest wywoływana przy dowolnym kliknięciu w tabeli, tj. również przy kliknięciu w nazwisko czy indeks.
        W takim wypadku po prostu nie wprowadza zmian.

        @param row: wiersz klikniętej komórki
        @param col: kolumna klikniętej komórki
        @return zmienia zawartość komórki
        """

        if self.StudentsList.item(row, col).text() == "nieobecność":
            self.StudentsList.item(row, col).setBackground(QtGui.QColor(0, 128, 0))
            self.StudentsList.item(row, col).setText("obecność")

        elif self.StudentsList.item(row, col).text() == "usprawiedliwienie":
            self.StudentsList.item(row, col).setBackground(QtGui.QColor(255, 69, 0))
            self.StudentsList.item(row, col).setText("nieobecność")

        elif self.StudentsList.item(row, col).text() == "obecność":
            self.StudentsList.item(row, col).setBackground(QtGui.QColor(218, 165, 32))
            self.StudentsList.item(row, col).setText("usprawiedliwienie")
