import sqlite3


class EgzaminDatabaseHandler:
    """
    @brief Klasa ułatwiająca połączenie z bazą danych dla pluginu egzamin
    """
    def __init__(self, database):
        """
        Metoda inicjująca instancję klasy.
        odpowiada za przypisanie wartości atrybutów klasy.
        @param database: Połączenie do bazy danych (DatabaseHandler)
        """
        self.database = database.getDatabase()
        self.parentDatabase=database

    def getTabIdList(self, CourseID):
        """
        Zwraca listę zakładek dla danego kursu
        @param CourseID:
        @return: Listę
        """
        sql = 'select `ID`, `Name` from `ExamList` where `CourseID`=\'' + str(CourseID) + '\''
        cursor = self.database.execute(sql)
        result = []
        for row in cursor:
            result.append({'id': row[0], 'name': row[1]})
        return result

    def createExamListTable(self):
        """
        Tworzy pustą tabelę egzaminów
        """
        sql = """CREATE TABLE IF NOT EXISTS `ExamList` (
        `ID` INTEGER PRIMARY KEY AUTOINCREMENT,
        `CourseID` INTEGER NOT NULL,
        `Name` text NOT NULL
        );"""
        self.database.execute(sql)
        self.database.commit()

    def createExamQuestionsTable(self):
        """
        Tworzy pustą tabelę pytań egzaminów
        """
        sql = """CREATE TABLE IF NOT EXISTS `ExamQuestions` (
        `ID` INTEGER PRIMARY KEY AUTOINCREMENT,
        `ExamID` INTEGER NOT NULL,
        `Question` text NOT NULL,
        `MaxPoints` INTEGER NOT_NULL
        );"""
        self.database.execute(sql)
        self.database.commit()

    ###Tabela która zawiera informację o ocene w zależności od ilości punktów
    def createExamPointsTable(self):
        """
        Tworzy pustą tabelę która ziera informację o ocenie w zależności od ilości punktów
        """
        sql = """CREATE TABLE IF NOT EXISTS `ExamPoints` (
        `ID` INTEGER PRIMARY KEY AUTOINCREMENT,
        `ExamID` INTEGER NOT NULL,
        `MinPoints` INTEGER NOT_NULL,
        `MaxPoints` INTEGER NOT_NULL,
        `Note` REAL NOT_NULL
        );"""
        self.database.execute(sql)
        self.database.commit()

    def createExamNotesTable(self):
        """
        Tworzy pustą tabelę w której są zapisane punkty dla danego studenta i pytania dla danego pluginu
        """
        sql = """CREATE TABLE IF NOT EXISTS `ExamNotes` (
        `ID` INTEGER PRIMARY KEY AUTOINCREMENT,
        `QuestionID` INTEGER NOT NULL,
        `StudentID` INTEGER NOT NULL,
        `Points` INTEGER NOT NULL
        );"""
        self.database.execute(sql)
        self.database.commit()

    def calculateNote(self, TabID, Points):
        """
        Oblicza ocenę dla danej zakładki na podstawie zdobytych punktów
        @param TabID: Identyfikator zakładki
        @param Points: Ilość zdobytych punktów
        @return: Ocenę
        """
        sql="select `Note` FROM `ExamPoints`" \
            " WHERE ExamID={} AND `MinPoints` <= {} AND `MaxPoints`>={}  ORDER BY `Note` DESC"\
            .format(TabID,Points,Points)
        note =  self.database.execute(sql).fetchone()[0]
        return note

    def getTabName(self, TabID):
        """
        Zwraca nazwę zakładki
        @param TabID: Identyfikator zakładki
        @return: Nazwę zakładki
        """
        sql = 'select `Name` from `ExamList` where `ID`=\'' + str(TabID) + '\''
        cursor = self.database.execute(sql)
        return cursor.fetchone()[0]

    def setTabName(self, TabID, TabName):
        """
        Nadaję nazwę zakładce
        @param TabID: Identyfikator zakładki
        @param TabName: Nazwa Zakładki
        """
        sql = 'UPDATE `ExamList` SET `Name`=\''+TabName+'\' WHERE `ID`=\''+str(TabID)+'\';'
        self.database.execute(sql)
        self.database.commit()

    def addEmptyTab(self, CourseID):
        """
        Dodaję pustą zakładkę do bazy danych
        @param CourseID: Identyfikator kursu
        @return Identyfikator dodanej zakładki
        """
        sql = 'INSERT INTO `ExamList` (`CourseID`,`Name`) VALUES (\''+str(CourseID)+'\',\'Nazwa\')'
        cursor = self.database.execute(sql)
        self.database.commit()
        return cursor.lastrowid

    def removeTab(self,TabID):
        """
        Usuwa zakładkę z bazy danych
        @param TabID: Identyfikator zakładki
        """
        sql='DELETE FROM `ExamList` WHERE `ID`=\''+str(TabID)+'\''
        self.database.execute(sql)
        self.database.commit()
        #TODO Usuwanie danych z tabeli pytani i odpowiedzi

    def getCourseID(self, TabID):
        """
        Zwraca identyfikator kursu dla danej zakładki
        @param TabID: Identyfikator zakładki
        @return Identyfikator kursu
        """
        sql="SELECT `CourseID` FROM `ExamList` WHERE `ID`={}".format(TabID)
        cursor = self.database.execute(sql)
        return cursor.fetchone()[0]

    def getQuestions(self,TabID):
        """
        Zwraca listę pytań dla danej zakładki
        @param TabID: Identyfikator zakładki
        @return: Listę
        """
        sql = 'SELECT `ID`, `Question`, `MaxPoints` FROM ExamQuestions WHERE `ExamID`={}'.format(TabID)
        cursor = self.database.execute(sql)
        data = []
        for row in cursor:
            data.append({'ID':row[0], 'Question':row[1], 'MaxPoints':row[2] })
        return data

    def updateQuestions(self,ID,Questions, MaxPoints):
        """
        Aktualizuje pytania w bazie danych
        @param ID: Identyfikator pytania
        @param Questions: Pytanie
        @param MaxPoints: Maksymalna możliwa ilość punktów do zdobycia
        """
        sql = "UPDATE `ExamQuestions` SET `MaxPoints`={}, `Question`={} WHERE `ID`={};"\
            .format(MaxPoints,Questions,ID)
        self.database.execute(sql)
        self.database.commit()

    # Aktualizacja pytań wraz z dodawaniem oraz usuwaniem
    def updateQuestions(self, TabID, data):
        """
        Aktualizuje pytania w bazie danych
        jeśli data['ID'] jest ujemne pytanie zostanie usunięte
        @param ID: Identyfikator pytania
        @param data: Dane do aktualizacji
        """

        for row in data:
            if (row['ID'] == 'new'):  # id new oznacza że jest to nowy rekord
                self.addQuestion(TabID, row)
            elif (row['ID'] < 0):  # Oznacza że rekord został zaznaczony do usunięcia
                row['ID']=-row['ID']
                self.delQuestion(row)
            else:
                self.updateQuestion(row)
        self.database.commit()

    #sama aktualiacja pytań
    def updateQuestion(self,data):
        """
        Aktualizuje pytania w bazie danych
        @param data: Dane do aktualizacji
        """
        sql = "UPDATE `ExamQuestions` SET `MaxPoints`={}, `Question`='{}' WHERE `ID`={};"\
            .format(data['MaxPoints'],data['Question'],data['ID'])
        self.database.execute(sql)
        #self.database.commit()

    def addQuestion(self, TabID, data):
        """
        Dodaje pytanie do bazy danych
        @param TabID: Identyfikator zakładki
        @param data: Dane
        """
        sql = """INSERT INTO `ExamQuestions` (`ExamID`,`Question`,`MaxPoints`)\
        VALUES ('{}','{}','{}');""".format(str(TabID), data['Question'], str(data['MaxPoints']))
        self.database.execute(sql)
        self.fillStudentsNotes(TabID)
        #self.database.commit()

    def delQuestion(self,data):
        """
        Usuwa pytanie z bazy danych
        @param data: Dane
        @return:
        """
        sql="DELETE FROM `ExamQuestions` WHERE `ID`={};".format(data['ID'])
        self.database.execute(sql)
        #self.database.commit()

    def addNotes(self,studentID, questionsID):
        """
        Dodaje ilość zdobytych punktów przez studenta dla danego pytania
        @param studentID: Identyfikator studenta
        @param questionsID: Identyfikator pytania
        """
        sql = "INSERT INTO `ExamNotes` (`QuestionID`,`StudentID`,`Points`)" \
              "VALUES ({},{},{});".format(questionsID,studentID,0)
        self.database.execute(sql)

    def getNotes(self,studentID,questionsID):
        """
        Zwraca ilość zdobytych punktów przez studenta dla danego pytania
        @param studentID: Identyfikator studenta
        @param questionsID: Identyfikator Pytania
        @return:
        """
        sql="SELECT `ID`, `Points` FROM `ExamNotes` " \
            "WHERE `StudentID`={} AND `QuestionID`={}".format(studentID,questionsID)
        cursor = self.database.execute(sql)
        data=cursor.fetchone()
        return {'ID':data[0],'Value':data[1]}

    def updateNote(self,NoteID, Value):
        """
        Zmienia ilość punktów zdobytą przez studenta dla danego pytania
        @param NoteID: Identyfikator punktów
        @param Value: Ilość punktów
        """
        sql="UPDATE `ExamNotes` SET `Points`={} WHERE `ID`={};".format(Value,NoteID)
        self.database.execute(sql)
        self.database.commit()

    def fillStudentsNotes(self,TabID):
        """
        Wypełnia tabelę punktów zdobytych przez studenta
        @param TabID: Identyfikator zakładki
        """
        Questions=self.getQuestions(TabID)
        CourseID = self.getCourseID(TabID)
        for Question in Questions:
            for student in self.parentDatabase.getStudentsIDs(CourseID):
                self.addNotes(student,Question['ID'])

    def fillPointsTable(self,TabID):
        """
        Wypełnia tabelę punkty-ocena
        @param TabID: Identyfikator zakładki
        """
        sql="INSERT INTO `ExamPoints`(ExamID, MinPoints, MaxPoints, Note) VALUES({}, 0, 0, '2.0');" \
            .format(TabID)
        self.database.execute(sql)

        sql ="INSERT INTO `ExamPoints`(ExamID, MinPoints, MaxPoints, Note) VALUES({}, 0, 0, '3.0');" \
            .format(TabID)
        self.database.execute(sql)

        sql="INSERT INTO `ExamPoints`(ExamID, MinPoints, MaxPoints, Note) VALUES({}, 0, 0, '3.5');" \
            .format(TabID)
        self.database.execute(sql)

        sql="INSERT INTO `ExamPoints`(ExamID, MinPoints, MaxPoints, Note) VALUES({}, 0, 0, '4.0');" \
            .format(TabID)
        self.database.execute(sql)

        sql="INSERT INTO `ExamPoints`(ExamID, MinPoints, MaxPoints, Note) VALUES({}, 0, 0, '4.5');" \
            .format(TabID)
        self.database.execute(sql)

        sql="INSERT INTO `ExamPoints`(ExamID, MinPoints, MaxPoints, Note) VALUES({}, 0, 0, '5.0');" \
            .format(TabID)
        self.database.execute(sql)

        sql="INSERT INTO `ExamPoints`(ExamID, MinPoints, MaxPoints, Note) VALUES({}, 0, 0, '5.5');" \
            .format(TabID)
        self.database.execute(sql)

        self.database.commit()

    def getPointsTable(self,ExamID):
        """
        Pobiera tabelę punktów
        @param ExamID: Identyfikator zakładki
        @return:
        """
        sql="SELECT `Note`, `MinPoints`, `MaxPoints` FROM `ExamPoints` WHERE ExamID = '{}';".format(ExamID)
        cursor = self.database.execute(sql)
        data = []
        for row in cursor:
            data.append({'Note':row[0], 'MinPoints':row[1], 'MaxPoints':row[2] })
        return data

    def updatePointsTable(self,ExamID,data):
        """
        Aktualizuje tabelę punktów
        @param ExamID: Identyfikator zakładki
        @param data: Dane
        """
        for i in data:
            sql = "UPDATE `ExamPoints` SET  `MinPoints`={}, `MaxPoints`={} WHERE `Note`={} AND `ExamID`={};"\
            .format(i['MinPoints'],i['MaxPoints'],i['Note'], ExamID)
            self.database.execute(sql)
        self.database.commit()

