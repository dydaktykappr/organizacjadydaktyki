import sqlite3
import datetime
import re

class obecnoscDatabaseHandler:
    def __init__(self, database):
        self.database = database.getDatabase()
        self.parentDatabase=database

    def createAttendanceTable(self):
        """
        Tworzy tabelę obecności, jeżeli nie istniała wcześniej.

        Pola:
            ID: identyfikacja wpisu
            StudentID: identyfikator studenta
            CourseID: identyfikator grupy zajęciowej
            Date: data zajęć
            Attended: obecność na danych zajęciach, wartości: 0 - nieobecny, 1 - obecny, 2 - usprawiedliwiony
        """
        sql = """CREATE TABLE IF NOT EXISTS `Attendance` (
        `ID` INTEGER PRIMARY KEY AUTOINCREMENT,
        `StudentID` int(11) NOT NULL,
        `CourseID` int(11) NOT NULL,
        `Date` text NOT NULL,
        `Attended` int(11) NOT NULL
        );"""
        self.database.execute(sql)
        self.database.commit()

    def getTabIdList(self, CourseID):
        """
        Wyszukuje w bazie listę identyfikatorów zakładek obecności dla danego kursu.

        @param CourseID: identyfikator kursu
        @return listę słowników o kluczach @a id, @a name
        """
        sql = "select `ID`, `Name` from `AttendList` where `CourseID`={}".format(CourseID)
        cursor = self.database.execute(sql)
        result = []
        for row in cursor:
            result.append({'id': row[0], 'name': row[1]})
        return result

    def getAttendanceByCourse(self,CourseID):
        """
        Wyszukuje w bazie pełną listę obecności dla danego kursu. Studenci reprezentowani są poprzez identyfikatory.

        @param CourseID: identyfikator kursu
        @return listę słowników o kluczach @a StudentID, @a Data, @a Attended
        """
        sql = "SELECT `StudentID`, `Date`, `Attended` FROM `Attendance` WHERE `CourseID`={}".format(CourseID)
        cursor = self.database.execute(sql)
        data = []
        for row in cursor:
            data.append({'StudentID': row[0], 'Data': row[1], 'Attended': row[2]})
        return data

    def getAttendanceByLecture(self, CourseID, day):
        """
        Wyszukuje w bazie listę obecności na danym kursie w danym dniu. Studenci reprezentowani są poprzez identyfikatory.
        @param CourseID: identyfikator kursu
        @param day: data zajęć w formacie DD-MM-YY
        @return: listę słowników o kluczach @a StudentID, @a Attended
        """
        sql = "SELECT `StudentID`, `Attended` " \
              "FROM `Attendance` " \
              "WHERE `CourseID`={} AND `Date`='{}'".format(CourseID, day)
        cursor = self.database.execute(sql)
        data = []
        for row in cursor:
            data.append({'StudentID': row[0], 'Attended': row[1]})
        return data

    def getAttendanceDaysByCourse(self, CourseID):
        """
        Wyszukuje w bazie daty kolejnych zapisanych list obecności z danego kursu.

        @param CourseID: identyfikator kursu
        @return: listę dat w formacie DD-MM-YY
        """
        sql = "SELECT DISTINCT `Date` FROM `Attendance` WHERE `CourseID`={}".format(CourseID)
        cursor = self.database.execute(sql)
        data = []
        for row in cursor:
            data.append(row[0])
        return data

    def createAttendListTable(self):
        """
        Tworzy tablicę dla przechowywania listy zakładek typu "obecność" stworzonych w trakcie pracy programu.
        """
        sql = """CREATE TABLE IF NOT EXISTS `AttendList` (
        `ID` INTEGER PRIMARY KEY AUTOINCREMENT,
        `CourseID` INTEGER NOT NULL,
        `Name` text NOT NULL
        );"""
        self.database.execute(sql)
        self.database.commit()

    def addEmptyTab(self, CourseID):
        """
        Dodaje do bazy informację o nowej, pustej zakładce.
        @param CourseID: identyfikator kursu
        @return: identyfikator nowo stworzonej zakładki
        """
        sql = "INSERT INTO `AttendList` (`CourseID`,`Name`) VALUES ('{}','Obecność')".format(CourseID)
        cursor = self.database.execute(sql)
        self.database.commit()
        self.prepareDates(CourseID)
        return cursor.lastrowid

    def prepareDates(self, CourseID):
        """
        Przygotowuje zestaw dat kolejnych zajęć na podstawie terminu z csv. Wykorzystuje wyrażenie regularne do rozbicia
        terminu na elementy składowe: dzień, parzystość/nieparzystość tygodnia, godzina rozpoczęcia, godzina zakończenia.
        Obecnie metoda nie jest nigdzie wykorzystywana.
        @param CourseID: ID kursu, dla którego przygotowujemy terminarz
        """
        sql = "SELECT `Date` FROM `Courses` WHERE `ID`={}".format(CourseID)
        termin = self.database.execute(sql).fetchone()[0]
        mask = re.compile('(\D\D)\s(TN)?(TP)?\s?(\d\d:\d\d)-(\d\d:\d\d)')
        result = re.match(mask, termin)
        day = result.group(1)
        odd = result.group(2)
        even = result.group(3)
        start = result.group(4)
        end = result.group(5)

        how_many = 30
        if odd or even:
            how_many = 15

        return range(how_many)

    def getStudentsIDs(self, CourseId):
        """
        Wyszukuje w bazie listę studentów zapisanych na dany kurs.
        @param CourseId: identyfikator kursu
        @return lista identyfikatorów studentów
        """
        command = 'select `ID` from `Student_Course` where `CourseID`={}'.format(CourseId)
        students = self.database.execute(command)
        IDs = []
        for student_id in students:
            IDs.append(student_id[0])
        return IDs

    def getStudentInfo(self, StudentId):
        """
        Wyszukuje w bazie pełen zestaw danych studenta.
        @param StudentId: identyfikator studenta
        @return słownik o kluczach: @a names, @a surname, @a index
        """
        command = 'select `Name`, `Surname`, `Index` from `Students` where `ID`={}'.format(StudentId)
        student = self.database.execute(command)
        name = student.fetchone()
        return {'names': name[0], 'surname': name[1], 'index': name[2]}

    def getTabIdList(self, CourseID):
        """
        Wyszukuje w bazie zakładki obecności dla danego kursu.
        @param CourseID: identyfikator kursu
        @return lista słowników o kluczach: @a id, @a name
        """
        sql = "select `ID`, `Name` from `AttendList` where `CourseID`={}".format(CourseID)
        cursor = self.database.execute(sql)
        result = []
        for row in cursor:
            result.append({'id': row[0], 'name': row[1]})
        return result

    def getCourseID(self, TabID):
        """
        Wyszukuje w bazie identyfikator kursu.
        @param TabID: identyfikator zakładki
        @return identyfikator kursu
        """
        sql = "SELECT `CourseID` FROM `AttendList` WHERE `ID`={}".format(TabID)
        cursor = self.database.execute(sql)
        return cursor.fetchone()[0]

    def setTabName(self, TabID, TabName):
        """
        Zmienia nazwę istniejącej zakładki.
        @param TabID: identyfikator zakładki
        @param TabName: nowa nazwa zakładki
        """
        sql = "UPDATE `AttendList` SET `Name`='{}' WHERE `ID`={};".format(TabName,TabID)
        self.database.execute(sql)
        self.database.commit()
    
    def removeTab(self,TabID):
        """
        Usuwa dane zakładki z bazy.
        @param TabID: identyfikator zakładki
        """
        sql="DELETE FROM `AttendList` WHERE ID={}".format(TabID)
        self.database.execute(sql)
        self.database.commit()

    def saveRow(self, StudentID, CourseID, Date, Attended):
        """
        Zapisuje w tabeli Attendance wiersz opisujący obecność studenta na danych zajęciach
        @param StudentID: identyfikator studenta
        @param CourseID: identyfikator kursu
        @param Date: data zajęć
        @param Attended: status w formie słownej ("obecność", "nieobecność", "usprawiedliwienie")
        """
        sql = "INSERT INTO `Attendance` (StudentID, CourseID, Date, Attended) " \
              "VALUES ({}, {}, '{}', '{}')".format(StudentID, CourseID, Date, Attended)
        self.database.execute(sql)
        
    def commit(self):
        self.database.commit()

