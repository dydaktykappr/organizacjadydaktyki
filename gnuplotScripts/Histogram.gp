#Przyjmuje pliki w formie:
#
# etykieta1 etykieta2 etykieta3
# nawaKolumn dana1 dana2
# nawaKolumn dana1 dana2

set terminal png nocrop enhanced size 900,640 font "arial,12" 
set boxwidth 0.9 absolute
set style fill   solid 1.00 border lt -1
set key inside right top vertical Right noreverse noenhanced autotitle nobox
set style histogram clustered gap 1 title textcolor lt -1
set datafile missing '-'
set style data histograms
set xtics border in scale 0,0 nomirror autojustify
set xtics  norangelimit
set xtics   ()
#set yrange [ 0.0 : 6.0] noreverse nowriteback
set autoscale
set offsets graph 0, 0, 0.05, 0.05
set xlabel "%xlabel%"
set ylabel "%ylabel%"
x = 0.0
set output '%filename%'
set grid
set title "%title%" 
N = system("awk 'NR==1{print NF}' %datafile%")
plot for [column=2:N:1] '%datafile%'  u column:xtic(1) ti col
