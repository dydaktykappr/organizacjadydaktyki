SOURCEDIR := Plugins
PLUGIN_UI := $(shell find . -name  '*.ui')
PLUGIN_UI_PY := $(PLUGIN_UI:.ui=_ui.py)

all: $(PLUGIN_UI_PY)
	
$(PLUGIN_UI_PY): %_ui.py: %.ui
	pyuic4 $< -o $@

clean:
	find . -name '*_ui.py' -delete

run: all
	python3 main.py

debug: all
	python3 main.py --debug

doc:
	doxygen

install: all
	@./install.sh
