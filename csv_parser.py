import csv
import logging


def csv_parser(database, filename):
    database.createCoursesTable()
    database.createStudentsTable()
    database.createStudentCourseTable()

    data = open(filename, 'r', encoding='windows-1250')
    reader = csv.reader(data, delimiter=';')
    # Dodaje kurs
    for row in reader:
        try:
            Type = row[0]
            Value = row[1]
        except IndexError:
            continue
        if Type == 'Rok akademicki':
            Year = row[1]
        if Type == 'Typ kalendarza':
            Calendar_type = row[1]
        elif Type == 'Semestr':
            Semester = Value
        elif Type == 'Kod grupy':
            Group_code = Value
        elif Type == 'Kod kursu':
            Course_code = Value
        elif Type == 'Nazwa kursu':
            Name = Value
        elif Type == 'Termin':
            Date = Value
        elif Type == 'Prowadzący':
            Lecturer = Value

    courseID = database.insertCourse(Year, Semester, Group_code, Course_code, Name, Lecturer, Date, Calendar_type)

    # Dodaje studentów
    data.seek(0)
    for row in reader:
        try:
            Index = row[1]
            Surname = row[2]
            Name = row[3]
            Year = row[4]
            Semester = row[5]
            Subject = row[6]
            SubCourses = row[10]
            if (Index == 'Nr albumu'):
                continue

            studentID = database.insertStudent(Index, Name, Surname)
            database.insertStudentCourse(studentID, courseID, Year, Semester, Subject, SubCourses)
        except IndexError:
            continue

    database.commit()
    logging.info('Kurs zaimportowany')
    return courseID
