set terminal png nocrop enhanced size 900,640 font "arial,12" 
set boxwidth 0.9 absolute
set style fill   solid 1.00 border lt -1
set key inside right top vertical Right noreverse noenhanced autotitle nobox
set style histogram clustered gap 1 title textcolor lt -1
set datafile missing '-'
set style data histograms
set xtics border in scale 0,0 nomirror autojustify
set xtics  norangelimit
set xtics   ()
set yrange [ 0.0 : 6.0] noreverse nowriteback
set xlabel "Semestr"
set ylabel "Ocena"
x = 0.0
set output 'HistogramOcen.png'
set grid
set title "Oceny wg semestru dla kursu Robotyka(2)" 
plot 'histogramData.dat' using 2:xtic(1) ti col, '' u 3 ti col, '' u 4 ti col

