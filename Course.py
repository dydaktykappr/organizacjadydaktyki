from ui.course_ui import Ui_Course
from PyQt4 import QtCore, QtGui
from Plugins import *
import logging
from Oceny import OcenyWindow

class Course(QtGui.QMainWindow):
    """
    Klasa okna kursu
    """
    def __init__(self, database, pluginList, CourseId, parent=None):
        """
        @param database: uchwyt do bazy
        @param pluginList: list pluginów
        @param CourseId: id kursu
        @param parent: Qt parent
        """
        super(Course, self).__init__(parent)
        self.ui = Ui_Course()
        ## @var ID
        # id kursu
        self.ID = CourseId
        ## @var database
        # uchwyt do bazy
        self.database = database
        self.ui.setupUi(self)
        self.connect(self.ui.tabWidget, QtCore.SIGNAL('tabCloseRequested (int)'), self.removeTab)
        ## @var MenuSignalMapper
        # mapuje wywołanie opcje menu na dodanie zakładki z nazwą
        self.MenuSignalMapper = QtCore.QSignalMapper(self)
        self.connect(self.MenuSignalMapper, QtCore.SIGNAL('mapped(const QString&)'), self.addTab)
        self.createMenu(pluginList)
        self.ui.ocenyButton.clicked.connect(self.openNotesWindow)
        ## @var tabs
        # zakładki
        self.tabs = []
        ## @var notes
        # okno ocen
        self.notes = OcenyWindow(self.database, self.ID, self.tabs)
        self.notes.setWindowTitle('Oceny')

    def createMenu(self, pluginList):
        """
        Tworzy menu kursu
        @param pluginList: nazwy zakładek
        """
        for name in pluginList:
            if name == 'KursyTowarzyszacePlugin':
                continue
            action = QtGui.QAction(name, self)
            self.connect(action, QtCore.SIGNAL('triggered()'), self.MenuSignalMapper, QtCore.SLOT('map()'))
            self.MenuSignalMapper.setMapping(action, name)
            self.ui.menuDodaj.addAction(action)

    def appendTab(self, pluginTab, name):
        """
        Dodaje nową zakładkę
        @param pluginTab: zakładka (QtTab)
        @param name: nazwa zakładki
        """
        self.tabs.append({'name': name, 'tab': pluginTab})
        self.ui.tabWidget.addTab(pluginTab, name)

    def addTabToCourse(self, plugin, tab):
        """
        Dodaje istniejącą zakładkę do GUI
        @param plugin: nazwa plugonu
        @param tab: słownik informacji o zakładce
        """
        pluginTab = eval(plugin)(self.database, tab['id'])
        self.appendTab(pluginTab, tab['name'])
        logging.info("{} plugin stworzony".format(tab['name']))
        logging.debug("{} ma typ {}".format(tab['name'], eval(plugin)))

    def addTab(self, pluginName):
        """
        Dodaje nową zakładkę do kursu
        @param pluginName: nazwa plugin
        """
        tabId = eval(pluginName).addId(self.database,self.ID)
        tab = eval(pluginName)(self.database, tabId)
        tabName = tab.configure()
        self.appendTab(tab, tabName)
        logging.info("{} plugin został stworzony".format(tabName))
        logging.debug("{} ma typ {}".format(tabName, eval(pluginName)))

    def removeTab(self, TabIndex):
        """
        Usuwa zakładkę
        @param TabIndex: indeks zakładki w GUI
        """
        reply = QtGui.QMessageBox.question(None, 'Message',
                                           "Jesteś pewny?", QtGui.QMessageBox.Yes |
                                           QtGui.QMessageBox.No, QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            tab = self.ui.tabWidget.widget(TabIndex)
            tab.closeTab()
            for tabInfo in self.tabs:
                if tabInfo['tab'] == tab:
                    tabToRemove = tabInfo
            self.tabs.remove(tabToRemove)
            logging.debug('Tab was removed from tabList: {}'.format(tabToRemove))
            self.ui.tabWidget.removeTab(TabIndex)
            logging.info('Zakładka usunięta')

    def openNotesWindow(self):
        """
        Otwiera okno ocen
        """
        self.notes.configureTable()
        self.notes.show()
