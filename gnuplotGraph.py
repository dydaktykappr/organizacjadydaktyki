#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from subprocess import call

class gnuplotGraph():
    """
    @brief Jest interfejsem do połaczenia z gnuplotem
    @details Uruchamia skrypty gnuplota
    @details dostępne parametry dla skryptów gnuplota: \%filename%, \%xlabel%, \%ylabel%, \%title%
    """
    def __init__(self):
        #@var filename
        ##Nawa pliku wynikowego
        self.filename="graph.png"

        #@var data
        ##Dane do wykresu (dwuwymiarowa tablica)
        self.data=[[]]

        #@var dataLabels
        ##Nagłówki kolumn (tablica)
        self.dataLabels=[]

        #@var gnuplotScript
        ##Nazwa skryptu gnuplot-a który zostanie uruchomiony
        self.gnuplotScript=""

        #@var xlabel
        ##Etykieta osi x
        self.xlabel=""

        #@var ylabel
        ##Etykieta osi y
        self.ylabel = ""

        #@var title
        ##Tytuł wykresu
        self.title = ""

        #@var tmpLocation
        ##Miejsce przechowywania plików tymczasowych
        self.tmpLocation="/tmp"

    def _prepareGnuplotScript(self):
        f = open(self.gnuplotScript, 'r')
        gpcommand = f.read()
        gpcommand = gpcommand.replace("%xlabel%", self.xlabel)
        gpcommand = gpcommand.replace("%ylabel%", self.ylabel)
        gpcommand = gpcommand.replace("%title%", self.title)
        gpcommand = gpcommand.replace("%filename%", self.filename)
        gpcommand = gpcommand.replace("%datafile%", self.tmpLocation+"/data.dat")
        gpcommand = gpcommand.replace("%datafile%", self.tmpLocation + "/data.dat")

        script = open(self.tmpLocation+"/script.gp",'w')
        script.write(gpcommand)

    def _saveData(self):
        dataFile = open(self.tmpLocation+"/data.dat",'w')
        for label in self.dataLabels:
            dataFile.write(str(label))
            dataFile.write(' ')
        dataFile.write('\n')

        for row in self.data:
            for cell in row:
                dataFile.write(str(cell))
                dataFile.write(' ')
            dataFile.write('\n')

    def generateGraph(self):
        self._prepareGnuplotScript()
        self._saveData()
        call(["gnuplot",self.tmpLocation+"/script.gp"])

