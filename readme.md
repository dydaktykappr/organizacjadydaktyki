Dydaktyka
===================================

### Zależności:
- python3
- pyqt4-dev
- pyqt4-dev-tools
- doxypy
- doxygen
- gnuplot

### Instalacja:
```sh
# Zbudowanie projektu:
 make all

# Uruchomienie:
make run
# lub
./main.py

# Instalacja:
sudo make install
```
### Opis plików:
makefile:

    Tworzy pliki py z ui
    
main.py:

    Główny plik
    
course.py:

    Plik wygenerowany z course.ui,
    Formularz na którym są dane danego kursu

mainwindow.py:

    Plik wygenerowany z mainwindow.ui
    Widok głównego okna programu
    
database.db:

    Baza danych SQLite
    
ui/course.ui:

    Widok formularza kursu

ui/mainwindow.ui:

    Widok głównego okna programu

Exams/Egzamin.py:

    Plugin zwykłego egzaminu, musimy dokładnie
    zdefiniować co ma udostępniać taki plugin
    Proponuję:
        Tabelę/Kwerendę student-ocena
        Widok zakładki która będzie widoczna w kursie
        Okno do zarządzania ustawieniami
    Jako plugin proponuję również obecność

### Skrócony opis bazy danych:
Do podglądu/edytowania bazy danych używano sqlitebrowser.

Baza danych zawiera 11 tabel:

@b AttendList:

    Tabela przechowująca informacje o stworzonych zakładkach obecności.

    Pola:

	ID - identyfikator zakładki

	CourseID - identyfikator kursu

	Name - nazwa zakładki

@b Attendance:

    Tabela przechowująca wszystkie listy obecności.

    Pola:

	ID - identyfikator wpisu

	StudentID - identyfikator studenta

	CourseID - identyfikator kursu

	Date - data zajęć, której dotyczy dany wpis

	Attended - status obecności, standardowo jeden z trzech: "obecność", "nieobecność", "usprawiedliwienie".

@b Courses:

    Tabela przechowująca dane na temat kursów (dane są pobierane z plików csv). 

    Pola:

	ID - identyfikator kursu

	Year - rok akademicki (RRRR/RRRR)

	Semester - semestr (letni lub zimowy)

	Group_code - kod grupy w systemie uczelnianym

	Course_code - kod kursu w systemie uczelnianym

	Name - nazwa kursu

	Lecturer - nazwisko wykładowcy

	Date - data zajęć w formacie DD [TN/TP] GG:MM-GG:MM

	Calendar_type - typ kalendarza (zwykle dwusemestralny)

@b ExamList:

    Tabela przechowująca informacje o zakładkach egzaminów.

    Pola:

	ID - identyfikator zakładki

	CourseID - identyfikator kursu

	Name - nazwa zakładki

@b ExamNotes:

    Tabela przechowująca informacje o wynikach egzaminacyjnych studentów.

    Pola:

	ID - identyfikator wpisu

	QuestionID - identyfikator pytania egzaminacyjnego

	StudentID - identyfikator studenta

	Points - liczba zdobytych punktów


@b ExamPoints:

    Tabela przechowująca informacje o rozkładzie punktacji w ramach egzaminu.
   
    Pola:

	ID - identyfikator wpisu

	ExamID - identyfikator egzaminu

	MinPoints - minimalny próg punktowy na daną ocenę

	MaxPoints - maksymalny próg punktowy na daną ocenę

	Note - ocena przysługująca za punktację mieszczącą się w zakresie [MinPoints, MaxPoints]


@b ExamQuestions:

    Tabela przechowująca informacje o pytaniach egzaminacyjnych.

    Pola:

	ID - identyfikator wpisu

	ExamID - identyfikator egzaminu

	Question - treść pytania

	MaxPoints - maksymalna ilość punktów do zdobycia za dane pytanie


@b Student_Course:

    Tabela przechowująca powiązania pomiędzy studentami i kursami (kto jest zapisany na jaki kurs).

    Pola:

	ID - identyfikator wpisu

	CourseID - identyfikator kursu

	StudentID - identyfikator studenta

	Year - numer roku studiów, na którym student realizuje dany kurs

	Semester - semestr realizacji kursu

	Subject - przedmiot kształcenia

	SubCourses - powiązane kursy cząstkowe 


@b Students:

    Tabela przechowująca dane osobowe studentów.

    Pola:

	ID - identyfikator studenta

	CourseID - identyfikator kursu

	Name - imię (imiona) studenta

	Surname - nazwisko studenta


@b SubCoursesList:

    Tabela przechowująca informacje o kursach cząstkowych.

    Pola:

	ID - identyfikator wpisu

	CourseID - identyfikator kursu

	Name - nazwa kursu cząstkowego

	Index - pozycja kursu cząstkowego w stringu dostarczonym w pliku csv

