DESKTOP=`xdg-user-dir DESKTOP`
if test `whoami` != "root"
then 
	echo "Instalację musisz uruchomić jako administrator !!!"
else 
 cp -rf . /usr/local/dydaktyka/
 chown $SUDO_USER:$SUDO_USER -R /usr/local/dydaktyka/
 echo "Zainstalowano w:/usr/local/dydaktyka/"
 echo "[Desktop Entry]" > $DESKTOP/dydaktyka.desktop
 echo "Version=1.0" >> $DESKTOP/dydaktyka.desktop
 echo "Terminal=false" >> $DESKTOP/dydaktyka.desktop
 echo "Icon=/usr/local/dydaktyka/icon.png" >> $DESKTOP/dydaktyka.desktop
 echo "Name=Dydaktyka" >> $DESKTOP/dydaktyka.desktop
 echo "Exec=/usr/local/dydaktyka/main.py" >> $DESKTOP/dydaktyka.desktop
 echo "Path=/usr/local/dydaktyka/" >> $DESKTOP/dydaktyka.desktop
 echo "Type=Application" >> $DESKTOP/dydaktyka.desktop
 chown $SUDO_USER:$SUDO_USER $DESKTOP/dydaktyka.desktop
 chmod +x $DESKTOP/dydaktyka.desktop
 echo "Utworzono skrót w:" $DESKTOP
fi



