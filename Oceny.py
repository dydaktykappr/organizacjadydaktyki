import csv
import datetime
import os
import sys
import logging
from PyQt4 import QtGui
from ui.Statystyki_ui import Ui_Statystyki
from ui import Oceny_tab_ui as ui
from Plugins.abstract_plugin import NoteNotAvailable
from gnuplotGraph import gnuplotGraph

def round_of_rating(number):
    """
    Zaokrągla ocenę do połówek
    @return Ocena zaokrąglona do połówki
    """
    return round(number * 2) / 2

class Statystyki(QtGui.QDialog, Ui_Statystyki):
    """
    Okno ze statytykami
    """
    def __init__(self, parent=None, database=None, CourseId=None):
        """
        Otwiera okno ze statystykami, uruchamia skrypt Gnuplota
        @param parent rodzic nowej zakładki, Qt parent
        @param database: uchwyt do bazy
        """
        QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)
        self.database=database
        self.CourseId=CourseId
        self.setRowsContent()
        self.calculateStats()
        self.buttonSemestr.clicked.connect(self.saveHistogramSemester)
        self.buttonNote.clicked.connect(self.saveHistogramNotes)

    def setRowsContent(self):
        """
        Wstawia dane do tabeli w oknie statystyk
        """
        data = self.database.getSemester(self.CourseId)

        self.statView.setRowCount(len(data)+1)
        commonStats = self.calculateStats()
        liczbaStudentów = commonStats["L. studentów"]
        średnia = commonStats["Średnia"]
        Min = commonStats["Min"]
        Max = commonStats["Max"]
        roundŚrednia = round(średnia * 100) / 100
        item = QtGui.QTableWidgetItem("Łącznie")
        self.statView.setItem(0,0,item)
        item = QtGui.QTableWidgetItem(str(liczbaStudentów))
        self.statView.setItem(0,1,item)
        item = QtGui.QTableWidgetItem(str(roundŚrednia))
        self.statView.setItem(0,2,item)
        item = QtGui.QTableWidgetItem(str(Min))
        self.statView.setItem(0,3,item)
        item = QtGui.QTableWidgetItem(str(Max))
        self.statView.setItem(0,4,item)
        index = 1
        for row in data:
            commonStats=self.calculateStatsSemester(row['Semestr'])
            Min = commonStats["Min"]
            Max = commonStats["Max"]
            roundŚrednia = round(commonStats['Średnia'] * 100) / 100

            item = QtGui.QTableWidgetItem(str(row['Semestr']))
            self.statView.setItem(index, 0, item)
            item = QtGui.QTableWidgetItem(str(row['L. studentów']))
            self.statView.setItem(index, 1, item)

            item = QtGui.QTableWidgetItem(str(roundŚrednia))
            self.statView.setItem(index, 2, item)
            item = QtGui.QTableWidgetItem(str(Min))
            self.statView.setItem(index, 3, item)
            item = QtGui.QTableWidgetItem(str(Max))
            self.statView.setItem(index, 4, item)

            index+=1

    def saveHistogramSemester(self):
        """
        Tworzy plik .png ze statystykami w zależności od semestru studenta
        """
        data = []
        semesterData = self.database.getSemester(self.CourseId)
        commonStats = self.calculateStats()
        średnia = commonStats["Średnia"]
        Min = commonStats["Min"]
        Max = commonStats["Max"]
        data.append(["Łącznie",Min,średnia,Max])
        index = 1
        for row in semesterData:
            semesterStats=self.calculateStatsSemester(row['Semestr'])
            Semestr = row['Semestr']
            Min = semesterStats["Min"]
            Max = semesterStats["Max"]
            roundŚrednia = round(semesterStats['Średnia'] * 100) / 100
            data.append([Semestr,Min,roundŚrednia,Max])
            index += 1

        print('Wybierz nazwę dla pliku .png z histogramem dla semestru')
        openDialog = QtGui.QFileDialog(self)
        filename = openDialog.getSaveFileName(filter='*.png')
        try:
            graph = gnuplotGraph()
            graph.gnuplotScript = 'gnuplotScripts/Histogram.gp'
            graph.filename = filename
            graph.data = data
            graph.dataLabels = ['Semestr','Średnia','Min','Max']
            graph.title = 'Histogram ocen końcowych w zależności od semestru'
            graph.xlabel="Semestr"
            graph.ylabel="Ocena"
            graph.generateGraph()
        except:
            pass

    def saveHistogramNotes(self):
        """
        Tworzy plik .png z histogramem rozkładu ocen końcowych wszystkich studentów
        """
        notes = []
        data = []
        students = self.getNotes()
        for s_id in students:
            notes.append(s_id['note'])
        for note in set(notes):
            num=notes.count(note)
            data.append([note,num])
        data.sort()

        print('Wybierz nazwę dla pliku .png z histogramem ocen')
        openDialog = QtGui.QFileDialog(self)
        filename = openDialog.getSaveFileName(filter='*.png')
        try:
            graph = gnuplotGraph()
            graph.gnuplotScript='gnuplotScripts/Histogram.gp'
            graph.filename=filename
            graph.data=data
            graph.dataLabels=['-','-']
            graph.title='Histogram ocen końcowych'
            graph.xlabel="Ocena"
            graph.ylabel="Liczba ocen"
            graph.generateGraph()
        except:
            pass

    def getNotes(self):
        """
        Zwraca słownik ID:ocena końcowa dla studentów wszystkich semestrów na danym kursie
        @return Słownik ID:ocena końcowa dla studentów wszystkich semestrów na danym kursie
        """
        students = []
        students_ids = self.parent().mainHandler.getStudentsIDs(self.parent().CourseID)
        for s_id in students_ids:
            notes = self.parent().getNotesFromTabs(s_id)
            try:
                note = self.parent().countFinalNote(notes)
                students.append({'id': s_id, 'note': note})
            except ZeroDivisionError:
                students.append({'id': s_id, 'note': ''})
        return students

    def getNotesFromStudentIDs(self,StudentIDs):
        """
        Zwraca słownik ID:ocena końcowa dla studentów danego semestru na danym kursie
        @param StudentIDs: Lista ID studentów jednego semestru
        """
        students=[]
        for s_id in StudentIDs:
            notes = self.parent().getNotesFromTabs(s_id)
            try:
                note = self.parent().countFinalNote(notes)
                students.append({'id': s_id, 'note': note})
            except ZeroDivisionError:
                students.append({'id': s_id, 'note': ''})
        return students


    def calculateStatsSemester(self,semester):
        """
        Wylicza statystyki dla studentów danego semestru na danym kursie
        @param semester: Semestr dla którego wyliczane są statystyki
        @return Słownik ze statystykami studentów danego semestru na danym kursie
        """
        studentIDs=self.database.getStudentIdsFromSemester(self.parent().CourseID,semester)
        students = self.getNotesFromStudentIDs(studentIDs)
        sumNote = 0
        studentsNumber = 0
        minNote = 7
        maxNote = -1
        index=0
        for s_id in students:
            try:
                currentNote = s_id['note']
                sumNote += currentNote
                studentsNumber += 1
                if currentNote > maxNote:
                    maxNote = currentNote
                if currentNote < minNote:
                    minNote = currentNote
            except ZeroDivisionError:
                pass
        try:
            meanNote = sumNote/studentsNumber
        except ZeroDivisionError:
            meanNote = "Brak studentów"
        stats = {"L. studentów":studentsNumber, "Średnia":meanNote, "Min":minNote, "Max":maxNote}
        return stats


    def calculateStats(self):
        """
        Wylicza statystyki wspólne dla studentów wszystkich semestrów na danym kursie
        @return Słownik ze wspólnymi statystykami dla studentów wszystkich semestrów na danym kursie
        """
        students = self.getNotes()
        sumNote = 0
        studentsNumber = 0
        minNote = 7
        maxNote = -1
        index=0
        for s_id in students:
            try:
                currentNote = s_id['note']
                sumNote += currentNote
                studentsNumber += 1
                if currentNote > maxNote:
                    maxNote = currentNote
                if currentNote < minNote:
                    minNote = currentNote
            except ZeroDivisionError:
                pass
        try:
            meanNote = sumNote/studentsNumber
        except ZeroDivisionError:
            meanNote = "Brak studentów"
        stats = {"L. studentów":studentsNumber, "Średnia":meanNote, "Min":minNote, "Max":maxNote}
        return stats

class OcenyWindow(QtGui.QWidget):
    """
    @brief Klasa okna ocen

    @details Klasa wyświetla oceny studentów
    """
    def __init__(self, database, CourseID, Tabs, parent=None):
        QtGui.QWidget.__init__(self, parent)
        ## @var mainHandler
        # uchwyt do bazy do obiektu bazy danych
        self.mainHandler = database
        self.ui = ui.Ui_tab()
        self.ui.setupUi(self)
        ## @var CourseID
        # id kursu
        self.CourseID = CourseID
        ## @var Tabs
        # wszystkie zakładki
        self.Tabs = Tabs
        self.weigthSpinBoxes = []
        self.configureTable()
        self.ui.refreshButton.clicked.connect(self.configureTable)
        self.ui.genNotesButton.clicked.connect(self.exportNotes)
        self.ui.exportAsTextButton.clicked.connect(self.exportNotesAsText)
        self.ui.was2_checkBox.stateChanged.connect(self.configureTable)
        self.ui.filter.textChanged.connect(self.configureTableWithFilter)
        self.ui.statsButton.clicked.connect(self.openStats)

    def openStats(self):
        """
        Otwiera okno statystyk
        """
        Stats = Statystyki(self, self.mainHandler, self.CourseID)
        Stats.show()

    def configureTable(self):
        """
        Metoda wypełniająca tabelę studentami i ocenami
        """
        students_ids = self.mainHandler.getStudentsIDs(self.CourseID)
        self.refreshWeightBoxes()
        self.prepareHeader(len(students_ids))
        row_index = 0
        for student in students_ids:
            self.setRow(row_index, student)
            row_index += 1

    def configureTableWithFilter(self, filter):
        """
        Metoda wypełniająca tabelę studentami i ocenami
        @param filter - filtr dla listy studentów
        """
        students_ids = self.mainHandler.getStudentsFilterList(self.CourseID, filter)
        self.refreshWeightBoxes()
        self.prepareHeader(len(students_ids))
        row_index = 0
        for student in students_ids:
            self.setRow(row_index, student['ID'])
            row_index += 1

    def refreshWeightBoxes(self):
        """
        Odświeża spinBox z wagami dla poszczególnych ocen
        """
        diff = len(self.Tabs) - len(self.weigthSpinBoxes)
        if diff > 0:
            for i in range(diff):
                weigth = QtGui.QDoubleSpinBox()
                weigth.setValue(1)
                self.weigthSpinBoxes.append(weigth)
                self.ui.weightsBox.addWidget(weigth)
        else:
            for i in range(-diff):
                w = self.weigthSpinBoxes[-1]
                w.setVisible(False)
                self.ui.weightsBox.removeWidget(w)
                del self.weigthSpinBoxes[-1]

    def prepareHeader(self, row_count):
        """
        Metoda przygotowuje nagłówek tabeli
        @param row_count - ilość wierszy w tabeli
        """
        columns_number = 4 + len(self.Tabs)
        columns_header = ['Imiona', 'Nazwisko', 'Index']
        for noteInfo in self.Tabs:
            columns_header.append(noteInfo['name'])
        columns_header.append('Ocena')
        self.ui.table.setColumnCount(columns_number)
        self.ui.table.setRowCount(row_count)
        self.ui.table.setHorizontalHeaderLabels(columns_header)

    def setRow(self, index, studentID):
        """
        Metoda wypełnia wiersz studentem
        @param index - indeks wiersza w tabeli
        @param studentID - id studenta (w bazie danych)
        """
        info = self.mainHandler.getStudentInfo(studentID)
        current_column = self.setStudentInfo(index, info)
        notes = self.getNotesFromTabs(studentID)
        current_column = self.setNotesInTable(notes, index, current_column)
        try:
            note = self.countFinalNote(notes)
            item = QtGui.QTableWidgetItem(str(note))
            if note < 3:
                item.setBackground(QtGui.QColor(250, 0, 0))
        except ZeroDivisionError:
            item = QtGui.QTableWidgetItem("Suma wag wynosi 0")
        self.ui.table.setItem(index, current_column, item)

    def setStudentInfo(self, index, info):
        """
        metoda wypełnia w tabeli imiona, nazwisko i index w wierszu
        @param index: indeks wiersza
        @param info: słownik z danymi studenta
        @return
        """
        nameItem = QtGui.QTableWidgetItem(info['names'])
        surnameItem = QtGui.QTableWidgetItem(info['surname'])
        indexItem = QtGui.QTableWidgetItem(info['index'])
        column_index = 0
        column_index = self.appendItem(nameItem, index, column_index)
        column_index = self.appendItem(surnameItem, index, column_index)
        column_index = self.appendItem(indexItem, index, column_index)
        return column_index

    def appendItem(self, item, row_index, column_index):
        """
        uzupełnia pole wiersza
        @param item: QTableWidgetItem
        @param row_index: indeks wiersza
        @param column_index: indeks kolumny
        @return zinkrementowany indeks kolumny
        """
        self.ui.table.setItem(row_index, column_index, item)
        return column_index + 1

    def getNotesFromTabs(self, studentID):
        """
        metoda pobiera z zakładek oceny
        @param studentID: id studenta
        @return listę ocen studenta
        """
        notesForStudent = []
        for tab in self.Tabs:
            try:
                note = tab['tab'].getNoteForStudent(studentID)
                notesForStudent.append(note)
            except NoteNotAvailable:
                notesForStudent.append(None)
        return notesForStudent

    def setNotesInTable(self, notes, index, current_column):
        """
        wypełnia tabelę ocenami
        @param notes: lista ocen
        @param index: indeks wiersza
        @param current_column: indeks kolumny
        @return kolejny indeks kolumny do wypełnienia
        """
        for note in notes:
            if note is not None:
                item = QtGui.QTableWidgetItem(str(note))
                if note < 3:
                    item.setBackground(QtGui.QColor(250, 0, 0))
            else:
                item = QtGui.QTableWidgetItem("Brak")
            self.ui.table.setItem(index, current_column, item)
            current_column += 1
        return current_column

    def countFinalNote(self, notes):
        """
        Wylicza finalną ocenę. 
        @param notes: lista ocen
        @return zwraca ocenę
        """
        finalNote = 0
        weightsSum = 0
        i = 0
        for weight in self.weigthSpinBoxes:
            if notes[i] is not None:
                finalNote += weight.value() * notes[i]
                weightsSum += weight.value()
            i += 1

        note = round_of_rating(finalNote / weightsSum)
        if note < 3 or (2.0 in notes and not self.ui.was2_checkBox.isChecked()):
            note = 2.0
        return note

    def exportNotes(self):
        """
        eksportuje oceny do pliku csv
        """
        openDialog = QtGui.QFileDialog(self)
        filename = openDialog.getSaveFileName(filter='*.csv')
        students = []
        students_ids = self.mainHandler.getStudentsIDs(self.CourseID)
        for s_id in students_ids:
            notes = self.getNotesFromTabs(s_id)
            try:
                note = self.countFinalNote(notes)
                students.append({'id': s_id, 'note': note})
            except ZeroDivisionError:
                students.append({'id': s_id, 'note': ''})
        c = CSV_creator(filename, students, self.CourseID, self.mainHandler)
        c.create()

    def exportNotesAsText(self):
        """
        eksportuje oceny do pliku txt
        """
        openDialog = QtGui.QFileDialog(self)
        filename = openDialog.getSaveFileName(filter='*.txt')
        students_ids = self.mainHandler.getStudentsIDs(self.CourseID)
        try:
            with open(filename, 'w') as file:
                names = [tab['name'] for tab in self.Tabs]
                file.write('Dane'.ljust(50, '.') + ', '.join(names) + ' --- Ocena\n')
                for s_id in students_ids:
                    try:
                        notes = self.getNotesFromTabs(s_id)
                        note = self.countFinalNote(notes)
                        if not self.ui.passed.isChecked() and note > 2.0:
                            continue
                        if not self.ui.not_passed.isChecked() and note <= 2.0:
                            continue
                    except ZeroDivisionError:
                        logging.error('Nie mozna wyliczyc oceny, suma wag wynosi 0')
                        break

                    student_info = self.mainHandler.getStudentInfo(s_id)
                    info = '{} '.format(student_info['index'])
                    if self.ui.names_checkBox.isChecked():
                        info += '{} '.format(student_info['names'])
                    if self.ui.surname_checkBox.isChecked():
                        info += '{} '.format(student_info['surname'])
                    info = info.ljust(50, '.')
                    info += ', '.join(str(i) for i in notes)
                    info += ' --- {}'.format(note)
                    file.write(info + '\n')
        except FileNotFoundError:
            pass


class CSV_creator:
    """
    @brief Klasa dla generuje plik csv
    """
    def __init__(self, filename, students_with_note, courseID, database):
        """

        @param filename: nazwa pliku
        @param students_with_note: lista słowników z id studentów i ocenami końcowymi
        @param courseID: id kursu
        @param database: uchwyt do bazy danych
        """
        self.filename = filename
        self.students = students_with_note
        self.database = database
        self.courseID = courseID

    def create(self):
        """
        Tworzy plik csv
        """
        try:
            with open(self.filename, 'w', encoding='windows-1250', newline='\n') as csvfile:
                out = csv.writer(csvfile, delimiter=';')
                self.writeHeader(out)
                now = datetime.datetime.now()
                date = now.strftime("%Y-%m-%d")
                lp = 1
                for student in self.students:
                    s_info = self.database.getStudentInfo(student['id'])
                    s_c_info = self.database.get_student_course_info(student['id'], self.courseID)
                    out.writerow(
                        [lp, s_info['index'], s_info['surname'], s_info['names'], s_c_info['year'], s_c_info['semestr'],
                         s_c_info['subject'], student['note'], date, '', s_c_info['sub_courses']])
                    lp += 1
        except FileNotFoundError:
            pass

    def writeHeader(self, file):
        """
        Tworzy nagłowek pliku csv
        @param file: plik csv
        """
        courseInfo = self.database.get_course_info(self.courseID)
        file.writerow(['Politechnika Wrocławska'])
        file.writerow(['Rok akademicki', courseInfo['year']])
        file.writerow(['Typ kalendarza', courseInfo['calendar_type']])
        file.writerow(['Semestr', courseInfo['semestr']])
        file.writerow(['Kod grupy', courseInfo['group_code']])
        file.writerow(['Kod kursu', courseInfo['course_code']])
        file.writerow(['Nazwa kursu', courseInfo['name']])
        file.writerow(['Termin', courseInfo['date']])
        file.writerow(['Prowadzący', courseInfo['lecturer']])
        file.writerow(
            ['Lp.', 'Nr albumu', 'Nazwisko', 'Imiona', 'Rok', 'Semestr', 'Przedmiot kształcenia', 'Ocena (np. 3.0)',
             'Data (RRRR-MM-DD)', 'Komentarz',
             'Informacja o ocenach z kursów cząstkowych (informacje te nie są brane pod uwagę podczas importu)'])
