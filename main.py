#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
from PyQt4 import QtCore, QtGui
from ui.mainwindow_ui import Ui_MainWindow
from DatabaseHandler import DatabaseHandler
import csv_parser
import logging
import Plugins
from Plugins import *
import re
from Course import Course
import signal
import webbrowser
signal.signal(signal.SIGINT, signal.SIG_DFL)
from ui.Autorzy_ui import Ui_Autorzy

class MainWindow(QtGui. QMainWindow):
    """
    Główne okno programu
    """
    def __init__(self, parent=None):
        """
        @param parent: Qt parent
        """
        super(MainWindow, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        ## @var vbox
        # layout z przyciskami
        self.vbox = QtGui.QVBoxLayout()
        self.ui.groupBox.setLayout(self.vbox)
        ## @var buttonSignalMapper
        # mapuje przyciski i id kursów
        self.buttonSignalMapper = QtCore.QSignalMapper(self)
        self.connect(self.buttonSignalMapper, QtCore.SIGNAL('mapped(int)'), self.showCourse)
        self.ui.actionImportuj_CSV.triggered.connect(self.importCourse)
        ## @var pluginNamesList
        # lista nazw pluginów
        self.pluginNamesList = []
        ## @var database
        # uchwyt do bazy
        self.database = DatabaseHandler('database.db')
        self.database.createCoursesTable()
        self.loadCourseButtons()
        # Ładuje pluginy
        self.loadPluginNames()
        self.ui.actionPomoc.triggered.connect(self.openManual)
        self.ui.actionAutorzy.triggered.connect(self.openAutorzy)
        self.autorstwo=otworzAutorow()

    def openAutorzy(self):
        """
        Otwiera dialog z informacją o autorach
        """
        self.autorstwo.show()

    def openManual(self):
        """
        Otwiera Manual.pdf, o ile znajduje się w tym samym katalogu
        """
        webbrowser.open("Manual.pdf")

    def loadPluginNames(self):
        """
        Wczytuje nazwy dostępnych pluginów
        """
        for plugin in dir(Plugins):
            if re.match('.*Plugin', plugin):
                self.pluginNamesList.append(plugin)
        logging.debug("Lista pluginów: {}".format(', '.join(self.pluginNamesList)))

    def showCourse(self, CourseID):
        """
        Wyświetla okno kursu
        @param CourseID: id kursu
        """
        course = self.createCourse(CourseID)
        for plugin in self.pluginNamesList:
            tabs = eval(plugin).getTabIdList(self.database, CourseID)
            for tab in tabs:
                course.addTabToCourse(plugin, tab)
        course.show()

    def createCourse(self, CourseID):
        """
        Tworzy okno kursu
        @param CourseID: id kursu
        """
        form = Course(self.database, self.pluginNamesList, CourseID, self)
        course = self.database.getCourse(CourseID)
        window_name = "{} - {}".format(course['name'], course['code'])
        form.setWindowTitle(window_name)
        return form

    def importCourse(self):
        """
        importuje kurs z pliku csv
        """
        openDialog = QtGui.QFileDialog(self)
        fileName = openDialog.getOpenFileName(filter="Edukacja (*.csv)")
        try:
            courseID = csv_parser.csv_parser(self.database, fileName)
            self.createTabsForSubCourses(courseID)
            self.clearCourseButtons()
            self.loadCourseButtons()
        except FileNotFoundError:
            logging.warning("Nie można dodać kursu, plik nie istnieje")

    def createTabsForSubCourses(self, courseID):
        """
        Tworzy zakładki dla kursów towarzyszących
        @param courseID: id kursu
        """
        students_ids = self.database.getStudentsIDs(courseID)
        subCourseInfo = self.database.get_student_course_info(students_ids[0], courseID)['sub_courses']
        subcourses = getSubCoursesStrings(subCourseInfo)
        i = 0
        for s in subcourses:
            name = parseSubCourseInfo(s)['name']
            KursyTowarzyszacePlugin.create(courseID, self.database, name, i)
            i += 1

    def loadCourseButtons(self):
        """
        wczytuje przyciski otwierające kursy
        """
        for course in self.database.getCourseList():
            button = QtGui.QPushButton(course['name'] + ' ' + course['code'])
            self.vbox.addWidget(button)
            self.connect(button, QtCore.SIGNAL('clicked()'), self.buttonSignalMapper, QtCore.SLOT('map()'))
            self.buttonSignalMapper.setMapping(button, int(course['id']))
        self.vbox.addStretch()
        
    def clearCourseButtons(self):
        """
        Usuwa przyciski kursów
        """
        for i in range(self.vbox.count()):
            self.vbox.removeItem(self.vbox.itemAt(0))


def setLogging():
    """
    Ustawia format logów
    """
    FORMAT = '[%(asctime)7s] %(levelname)-7s - %(message)s'
    try:
        if sys.argv[1] == "--debug":
            level = logging.DEBUG
        else:
            level = logging.INFO
    except IndexError:
        level = logging.INFO
    logging.basicConfig(format=FORMAT, level=level, datefmt='%H:%M:%S')
    logging.debug("DEBUG włączony")

class otworzAutorow(QtGui.QDialog, Ui_Autorzy):
    """
    Okno z informacją o autorach
    """
    def __init__(self, parent=None):
        """
        @param parent rodzic nowej zakładki, Qt parent
        """
        QtGui.QDialog.__init__(self, parent)
        self.setupUi(self)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    setLogging()
    myapp = MainWindow()
    myapp.show()
    sys.exit(app.exec_())
