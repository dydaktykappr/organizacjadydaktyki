import logging
import sqlite3
import sys


class DatabaseHandler:
    """
    @brief Łącze do bazy danych
    @details Klasa która ułatwia połączenie z bazą danych
    """
    def __init__(self, name):
        """
        Metoda inicjująca instancję klasy.
        odpowiada za przypisanie wartości atrybutów klasy.
        @param name: Nazwa pliku bazy danych
        """
        ## @var database
        # połączenie do sqlite3
        self.database = sqlite3.connect(name, check_same_thread=False)

    def getSemester(self, CourseID):
        """
        Zwraca liczbę studentów z danego semestru na wybranym kursie
        @return: liczba studentów z danego semestru na wybranym kursie
        """
        sql= "SELECT `Semester`, COUNT(*) FROM student_course WHERE CourseID={} GROUP BY `Semester`".format(CourseID)
        cursor = self.database.execute(sql)
        result = []
        for row in cursor:
            result.append({'Semestr': row[0], 'L. studentów': row[1]})
        return result

    def getCourseList(self):
        """
        Zwraca listę wszystkich kursów
        @return: Listę wszystkich kursów
        """
        sql = "select `ID`, `Name`, `Group_code` from `Courses`"
        cursor = self.database.execute(sql)
        result = []
        for row in cursor:
            result.append({'id': row[0], 'name': row[1], 'code': row[2]})
        return result

    def getCourse(self, courseid):
        """
        Zwraca dane kursu (Nazwa, kod grupy)
        @param courseid identyfikator kursu
        @return Listę słowników
        """
        sql = "select `ID`, `Name`, `Group_code` from `Courses`where ID == {}".format(courseid)
        data = self.database.execute(sql)
        if data.arraysize != 1:
            logging.error("There is no course or more than 1 with id = {}".format(courseid))
            sys.exit(1)
        row = data.fetchone()
        return {'id': row[0], 'name': row[1], 'code': row[2]}

    # Struktura tabeli dla tabeli `Courses`
    def createCoursesTable(self):
        """
        Tworzy tabele kursu
        """
        sql = """CREATE TABLE IF NOT EXISTS `Courses` (
    `ID` INTEGER PRIMARY KEY AUTOINCREMENT,
    `Year` text NOT NULL,
    `Semester` text NOT NULL,
    `Group_code` text NOT NULL,
    `Course_code` int(11) NOT NULL,
    `Name` text NOT NULL,
    `Lecturer` text NOT NULL,
    `Date` text NOT NULL,
    `Calendar_type` text NOT NULL
    );"""
        self.database.execute(sql)
        self.database.commit()

    # Struktura tabeli dla tabeli `Students`
    def createStudentsTable(self):
        """
        Tworzy tabele studentów
        """
        sql = """CREATE TABLE IF NOT EXISTS `Students` (
    `ID` INTEGER PRIMARY KEY AUTOINCREMENT,
    `Index` text NOT NULL,
    `Name` text NOT NULL,
    `Surname` text NOT NULL
    );"""
        self.database.execute(sql)
        self.database.commit()

    # Struktura tabeli dla tabeli `Stuent_Course`
    def createStudentCourseTable(self):
        """
        Tworzy tabelę która łączy tabelę studentów z tabelą kursów
        @return:
        """
        sql = """CREATE TABLE IF NOT EXISTS `Student_Course` (
    `ID` INTEGER PRIMARY KEY AUTOINCREMENT,
    `CourseID` int(11) NOT NULL,
    `StudentID` int(11) NOT NULL,
    `Year` text NOT NULL,
    `Semester` text NOT NULL,
    `Subject` text NOT NULL,
    `SubCourses` text NOT NULL
    );"""
        self.database.execute(sql)
        self.database.commit()

    def checkCourseExist(self, Group_code):
        """
        Sprawdza czy dany kurs istnieje
        @param Group_code Kod grupy
        @return True jeśli istnieje lub False jeśli nie
        """
        sql = "select `Group_code` from `Courses` where `Group_code`='" + Group_code + "'"
        cursor = self.database.execute(sql)
        if len(cursor.fetchall()) == 0:
            return False
        else:
            return True

    def checkStudentExist(self, Index):
        """
        Sprawdza czy dany student istnieje
        @param Index Index studenta
        @return True jeśli istnieje lub False jeśli nie
        """
        sql = "select `Index` from `Students` where `Index`='" + Index + "'"
        cursor = self.database.execute(sql)
        if len(cursor.fetchall()) == 0:
            return False
        else:
            return True

    def checkStudentCourseExist(self, StudentID, CourseID):
        """
        Sprawdza czy istnieje połączenie kurs-student (czy dany student jest zapisany na kurs)
        @param StudentID: Identyfikator studenta
        @param CourseID: Identyfikator kursu
        @return: True jeśli istnieje lub False jeśli nie
        """
        sql = "select `ID` from `Student_Course` where `CourseID`='" + str(CourseID) + "' AND `StudentID`='" + str(
            StudentID) + "'"
        cursor = self.database.execute(sql)
        if len(cursor.fetchall()) == 0:
            return False
        else:
            return True

    def insertCourse(self, Year, Semester, Group_code, Course_code, Name, Lecturer, Date, Calendar_type):
        """
        Dodaje kurs do bazy danych
        @param Year: Rok
        @param Semester: Semestr
        @param Group_code:  Kod grupy
        @param Course_code: Kod kursu
        @param Name: Nazwa
        @param Lecturer: Prowadzący
        @param Date: Data
        @param Calendar_type: Typ kalendarza
        @return Identyfikator nowo dodanego kursu
        """
        # Jeśli taki kurs istnieje to go nie doda
        if not self.checkCourseExist(Group_code):
            sql = """INSERT INTO `Courses`
                (`Year`, `Semester`, `Group_code`, `Course_code`, `Name`, `Lecturer`, `Date`, `Calendar_type`)"""
            sql += "VALUES ('{Year}','{Semester}','{Group_code}','{Course_code}'," \
                   "'{Name}','{Lecturer}','{Date}', '{Calendar_type}');".format(Year=Year, Semester=Semester,
                                                                              Group_code=Group_code,
                                                                              Course_code=Course_code, Name=Name,
                                                                              Lecturer=Lecturer,
                                                                              Date=Date, Calendar_type=Calendar_type)
            self.database.execute(sql)
        # pobieram id dodanego rekordu
        sql = "select `ID` from `Courses` where `Group_code`='{Group_code}'".format(Group_code=Group_code)
        cursor = self.database.execute(sql)
        return cursor.fetchall()[0][0]

    def get_course_info(self, course_id):
        """
        Pobiera informację o kursie
        @param course_id: Identyfikator kursu
        @return Słownik
        """
        command = """select `Year`, `Semester`, `Group_code`, `Course_code`,
            `Name`, `Lecturer`, `Date`, `Calendar_type`
            from Courses
            where `ID`={}""".format(course_id)
        course_data = self.database.execute(command)
        course = course_data.fetchone()
        return {'year': course[0], 'semestr': course[1], 'group_code': course[2], 'course_code': course[3],
                'name': course[4], 'lecturer': course[5], 'date': course[6], 'calendar_type': course[7]}

    def insertStudent(self, Index, Name, Surname):
        """
        Dodaję nowego studenta
        @param Index: Numer indeksu
        @param Name: Imię
        @param Surname: Nazwisko
        """
        if not self.checkStudentExist(Index):
            # Dodaje rekord
            sql = "INSERT INTO `Students` (`Index`, `Name`, `Surname`)"
            sql += "VALUES ('" + Index + "','" + Name + "','" + Surname + "');"
            self.database.execute(sql)

        # pobieram id dodanego rekordu
        sql = "select `ID` from `Students` where `Index`='" + Index + "'"
        cursor = self.database.execute(sql)
        return cursor.fetchall()[0][0]

    def insertStudentCourse(self, StudentID, CourseID, Year, Semester, Subject, SubCourses):
        """
        Dodaję połączenie student - kurs
        @param StudentID: Identyfikator studenta
        @param CourseID: Identyfikator kursu
        @param Year: Rok w którym student jest zapisany na kurs
        @param Semester: Semestr w którym student jest zapisany na kurs
        @param Subject: Przedmiot kształcenia
        @param SubCourses: Kursy towarzyszące
        @return Identyfikator dodanego rekordu
        """
        if not self.checkStudentCourseExist(StudentID, CourseID):
            # Dodaje rekord
            sql = "INSERT INTO `Student_Course` (`CourseID`, `StudentID`, `Year`, `Semester`, `Subject`, `SubCourses`)"
            sql += "VALUES ('{}', '{}', '{}', '{}', '{}', '{}');".format(CourseID, StudentID, Year, Semester, Subject,
                                                                         SubCourses)
            self.database.execute(sql)

        # pobieram id dodanego rekordu
        sql = "select `ID` from `Student_Course` where `CourseID`='" + str(CourseID) + "' AND `StudentID`='" + str(
            StudentID) + "'"
        cursor = self.database.execute(sql)
        return cursor.fetchall()[0][0]

    def get_student_course_info(self, student_id, course_id):
        """
        Wzraca informację połączenu student - kurs
        @param student_id: Identyfikator studenta
        @param course_id: Identyfikator Kursu
        @return Słownik
        """
        command = """select `Year`, `Semester`, `Subject`, `SubCourses`
            from Student_Course
            where `CourseID`={} and `StudentID`={}""".format(course_id, student_id)
        course_data = self.database.execute(command)
        course = course_data.fetchone()
        return {'year': course[0], 'semestr': course[1], 'subject': course[2], 'sub_courses': course[3]}

    def commit(self):
        """
        Zapisuje wszystkie zmiany w bazie danych
        """
        self.database.commit()

    def getDatabase(self):
        """
        Zwraca połączenie do bazy danych sqlite3
        @return: referencję do sqlite3
        """
        return self.database

    def getStudentsIDs(self, CourseId):
        """
        Zwraca listę studentów zapisanych na dany kurs
        @param CourseId: Identyfikator kursu
        @return Listę
        """
        command = 'select `StudentID` from `Student_Course` where `CourseID`={}'.format(CourseId)
        students = self.database.execute(command)
        IDs = []
        for student_id in students:
            IDs.append(student_id[0])
        return IDs

    def getStudentsFilterList(self, CourseId, Filter):
        """
        Zwraca listę studentów zapisanych na dany kurs i którzy pasują do wzorca
        @param CourseId: Identyfikator kursu
        @param Filter: Wzorzec
        @return Listę
        """
        command = """SELECT * FROM `Students`, `Student_Course`
        WHERE `Students`.`ID`= `Student_Course`.`StudentID`
        AND `Student_Course`.`CourseID`={}
        AND (
        `Students`.`Index` LIKE "%{}%"
        OR `Students`.`Name` LIKE "%{}%"
        OR `Students`.`Surname` LIKE "%{}%")""".format(CourseId, Filter, Filter, Filter)
        students = self.database.execute(command)
        data = []
        for student in students:
            data.append({'ID': student[0], 'index': student[1], 'names': student[2], 'surname': student[3]})
        return data

    def getStudentInfo(self, StudentId):
        """
        Zwraca informację o studencie (Imię, Nazwisko, Index)
        @param StudentId: Identyfikator studenta
        @return: Słownik
        """
        command = 'select `Name`, `Surname`, `Index` from `Students` where `ID`={}'.format(StudentId)
        student = self.database.execute(command)
        name = student.fetchone()
        return {'names': name[0], 'surname': name[1], 'index': name[2]}

    def getStudentIdsFromSemester(self, CourseID, Semester):
        sql="SELECT `StudentID` FROM `Student_Course` WHERE `CourseID`='{}' AND `Semester`='{}'"\
            .format(CourseID,Semester)
        students = self.database.execute(sql)
        IDs = []
        for student_id in students:
            IDs.append(student_id[0])
        return IDs

